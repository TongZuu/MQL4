//+------------------------------------------------------------------+
//|                                   Korawit Zone Trading_Draft.mq4 |
//|                                                Korawit Pongsuwan |
//|                       https://www.facebook.com/tomatoze.investor |
//+------------------------------------------------------------------+
#property copyright "Korawit Pongsuwan"
#property link      "https://www.facebook.com/tomatoze.investor"
#property version   "1.00"
#property strict
  //---- input parameters for regular doji
extern bool      FindRegularDoji=true; //false to disable
extern int       MinLengthOfUpTail=3; //candle with upper tail equal or more than this will show up
extern int       MinLengthOfLoTail=3; //candle with lower tail equal or more than this will show up
extern double    MaxLengthOfBody=51.0; //candle with body less or equal with this will show up

extern bool       Dynamiclotsize = true;
extern bool       fiftlot = 1;
extern double Lot = 0.01 ;
bool       SellExtraHedging = 0,BuyExtraHedging = 0;
int BuyExtraHedgingfactor =0,SellExtraHedgingfactor = 0;

double Op,Cl,Hi,Lo,Ti ;
double pt ;
int num = 0;

struct DataTicket {
   int buyA;
   int buyB;
   int buyC;
   int sellA;
   int sellB;
   int sellC;
   DataTicket() { buyA=-1; buyB=-1; buyC=-1; sellA=-1; sellB=-1; sellC=-1; }
};

struct CountMarket{
	int buyMarket;
	int sellMarket;
	int totalMarket;
};


double UPBB , LOBB ,Sto,EMA100 ;

extern double Zonepitch = 5.00;


extern bool CheckOncePerBar = true;
datetime CurrentTimeStamp;
bool NewBar;
int BarShift = 1;
int MagicNumber = 1234 , MagicA = 1111 , MagicB = 2222 , MagicC = 3333;

bool BullSig , BareSig;

int sizeTicket = 60;
DataTicket myTicket[60];

double CurrentBuyProfit = 0, CurrentSellProfit = 0;

extern int qualitySell = 1170 , qualityBuy = 1270;

string Zone = "Zone 0";
string fileName = "ListTicket.bin";


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
//---
    pt = Point();
    CurrentTimeStamp = Time[0];
	loadHistory();
//---
   //return(INIT_FAILED);
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   Print("TongZuu >> OnDeinit");
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
	MqlTick last_tick;
	if(SymbolInfoTick(Symbol(),last_tick)){
		Hi = iHigh(Symbol(),0,1);
		Lo = iLow(Symbol(),0,1);
		Op = iOpen(Symbol(),0,1);
		Cl = iClose(Symbol(),0,1);
		Ti = iTime(Symbol(),0,1);

		UPBB = iBands(NULL,0,20,2,0,PRICE_CLOSE,MODE_UPPER,0);
		LOBB = iBands(NULL,0,20,2,0,PRICE_CLOSE,MODE_LOWER,0);
		Sto = iStochastic(NULL,0,5,3,3,MODE_EMA,0,MODE_MAIN,1);
		EMA100 = iMA(NULL,PERIOD_CURRENT,100,0,MODE_EMA,PRICE_WEIGHTED,0);

		CurrentBuyProfit = CurrentBuyProfitfn(myTicket[0].buyA , myTicket[1].buyA , myTicket[2].buyA , myTicket[3].buyA , myTicket[4].buyA);

		if(CheckOncePerBar == true){

		   if(CurrentTimeStamp != Time[0]) {
			   CurrentTimeStamp = Time[0];
			   NewBar = true;	
		   }else NewBar = false;
		}else {
			NewBar = true;
			BarShift = 0;
		}

		BullSig = (Hi-Cl>=MinLengthOfUpTail*pt && Cl-Lo>=MinLengthOfLoTail*pt  && MathAbs(Cl-Op)<=MaxLengthOfBody*pt && NewBar && (Hi+Lo+Lo+Lo+Cl+Cl )/6 < LOBB);
		BareSig = (Hi-Cl>=MinLengthOfUpTail*pt && Cl-Lo>=MinLengthOfLoTail*pt  && MathAbs(Cl-Op)<=MaxLengthOfBody*pt && NewBar && (Hi+Hi+Hi+Lo+Cl+Cl )/6 >UPBB);

		if(BullSig){
			  ObjectCreate(Symbol(),"Text_"+ num,OBJ_TEXT,0,Ti,Lo-100*pt);
			  ObjectSetText("Text_"+num,"Doji",7,"Times New Roman",Yellow);
			  num++;
		}else if(BareSig ){
			  ObjectCreate(Symbol(),"Text_"+num,OBJ_TEXT,0,Ti,Hi+100*pt);
			  ObjectSetText("Text_"+num,"Doji",7,"Times New Roman",Yellow);
			  num++;
		}

		int idxSell = MathFloor(((last_tick.bid - qualitySell) / Zonepitch)-1);
		//Print("--------------------- Index Sell : ["+idxSell +"] last_tick.bid : ["+last_tick.bid+"] ");
		if( idxSell >= 0 && idxSell < 60 ){
			if(myTicket[idxSell].sellA == -1 ){
				myTicket[idxSell].sellA = _OrderSend( Symbol() , OP_SELL , Lot , last_tick.bid , Red , "Sell A Index["+idxSell+"]" , MagicA);
			}
			if(myTicket[idxSell].sellB == -1 ){
				myTicket[idxSell].sellB = _OrderSend( Symbol() , OP_SELL , Lot , last_tick.bid , Red , "Sell B Index["+idxSell+"]" , MagicB);
			}
			if(myTicket[idxSell].sellC == -1 ){
				myTicket[idxSell].sellC = _OrderSend( Symbol() , OP_SELL , Lot , last_tick.bid , Red , "Sell C Index["+idxSell+"]" , MagicC);
			}
		}
		
		int idxBuy = MathFloor(((qualityBuy - last_tick.ask) / Zonepitch)-1);
		//Print("--------------------- Index Buy : ["+idxBuy +"] last_tick.last_tick.ask : ["+last_tick.ask+"] ");
		if(idxBuy >= 0 &&  idxBuy < 60){
			if(myTicket[idxBuy].buyA == -1 ){
				myTicket[idxBuy].buyA = _OrderSend( Symbol() , OP_BUY , Lot , last_tick.ask , Green , "Buy A Index["+idxBuy+"]" , MagicA);
			}
			if(myTicket[idxBuy].buyB == -1 ){
				myTicket[idxBuy].buyB = _OrderSend( Symbol() , OP_BUY , Lot , last_tick.ask , Green , "Buy B Index["+idxBuy+"]" , MagicB);
			}
			if(myTicket[idxBuy].buyC == -1 ){
				myTicket[idxBuy].buyC = _OrderSend( Symbol() , OP_BUY , Lot , last_tick.ask , Green , "Buy C Index["+idxBuy+"]" , MagicC);
			}
		}
		

		for(int index=0;index < sizeTicket; index++){
			
			if(myTicket[index].sellA != -1 &&  last_tick.ask < qualitySell + (Zonepitch * (index+0.8))){
				if( _OrderClose(myTicket[index].sellA , last_tick.ask , Red) ){
					myTicket[index].sellA = -1;
				}
			}
			if( last_tick.ask < EMA100 ){
				bool condCloseSell = last_tick.ask < qualitySell + (Zonepitch * (index));
				if(myTicket[index].sellB != -1 && condCloseSell ){
					if( _OrderClose(myTicket[index].sellB , last_tick.ask , Red) ){
						myTicket[index].sellB = -1;
					}
				}
				if(myTicket[index].sellC != -1 && condCloseSell && NewBar && last_tick.ask < LOBB  ){
					if( _OrderClose(myTicket[index].sellC , last_tick.ask , Red) ){
						myTicket[index].sellC = -1;
					}
				}
			}

			if(myTicket[index].buyA != -1 &&  last_tick.bid > qualityBuy - (Zonepitch * (index + 0.8)) ){
				if( _OrderClose(myTicket[index].buyA , last_tick.bid , Green) ){
					myTicket[index].buyA = -1;
				}
			}
			if(last_tick.bid > EMA100){
				bool condCloseBuy = last_tick.bid > qualityBuy - (Zonepitch * (index + 0));
				if(myTicket[index].buyB != -1 && condCloseBuy ){
					if( _OrderClose(myTicket[index].buyB , last_tick.bid , Green) ){
						myTicket[index].buyB = -1;
					}
				}
				if(myTicket[index].buyC != -1 && condCloseBuy  && NewBar && last_tick.bid > UPBB ){
					if( _OrderClose(myTicket[index].buyC , last_tick.bid , Green) ){
						myTicket[index].buyC = -1;
					}
				}
			}
		}
		
		CountMarket cm = getCountMarket();
		Comment("Upper BB Value = ", DoubleToStr(UPBB,2) ,"\n","Lower BB Value = ",DoubleToStr(LOBB,2),"\n","Stochastic Value = ",DoubleToStr(Sto,2),
		"\n","Total Order Count = ", cm.totalMarket,
		"\n","Buy Market Count = " , cm.buyMarket,
		"\n","Sell Market Count = ", cm.sellMarket,
		"\n","Current Sell Profit = " ,CurrentSellProfit
		,"\n","Current Buy Profit = " ,CurrentBuyProfit
		,"\n","Ask = " , DoubleToStr(last_tick.ask,2),
		"\n","Account Equity = " , AccountEquity() );
	}
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| expert Other function                                            |
//+------------------------------------------------------------------+

CountMarket getCountMarket(){
	CountMarket resultCountMarket;
	resultCountMarket.buyMarket = 0;
	resultCountMarket.sellMarket = 0;
	resultCountMarket.totalMarket = 0;
	for(int Counter = 0; Counter <= OrdersTotal()-1; Counter++){
		if(OrderSelect(Counter , SELECT_BY_POS)){
			int ordMagicNumber = OrderMagicNumber();
			bool checkMagic = (ordMagicNumber == MagicNumber || ordMagicNumber == MagicA || ordMagicNumber == MagicB || ordMagicNumber == MagicC);
			if(checkMagic && OrderSymbol() == Symbol()){
				resultCountMarket.totalMarket++;
				if( OrderType() == OP_BUY ){
					resultCountMarket.buyMarket++;
				}else if( OrderType() == OP_SELL ){
					resultCountMarket.sellMarket++;
				}
			}
		}
	}
	return resultCountMarket;
}

//+------------------------------------------------------------------+

double CurrentSellProfitfn( int myTicketfn1,int myTicketfn2,int myTicketfn3,int myTicketfn4,int myTicketfn5  ){
	double ProfitS1 = 0,ProfitS2 = 0,ProfitS3 = 0,ProfitS4 = 0;
	double SellProfit;

	if(SellMarketCount(Symbol(),MagicNumber) == 0){
		SellProfit = 0; 
	}else if(SellMarketCount(Symbol(),MagicNumber) == 1){
		SellProfit= 0; 
		OrderSelect(myTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 2){
		SellProfit= 0; 
		OrderSelect(myTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 3){
		SellProfit = 0; 
		OrderSelect(myTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 4){
		SellProfit = 0; 
		OrderSelect(myTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn4,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 5){
		SellProfit = 0; 
		OrderSelect(myTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn4,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(myTicketfn5,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}
	return(SellProfit);
}
//+------------------------------------------------------------------+

double CurrentBuyProfitfn(int BuyTicketfn1,int BuyTicketfn2,int BuyTicketfn3,int BuyTicketfn4,int BuyTicketfn5){
	double BuyProfit;
	if(BuyMarketCount(Symbol(),MagicNumber) == 0){
		BuyProfit = 0; 
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 1){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 2){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 3){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 4){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn4,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 5){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn4,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn5,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}
	return(BuyProfit);
}

int _OrderSend(string symbol , int ops , double volume , double price , color col ,string msg , int magic){
	return (OrderSend(symbol , ops , volume , price , 3 ,0,0, msg , magic , 0 , col));
}

bool _OrderClose(int ticket , double price ,  color col){
	int num = 0;
	bool resultReturn = false;
	do{
		num++;
		bool resultSelect = OrderSelect(ticket , SELECT_BY_TICKET);
		if( resultSelect ){
			bool resultClose = OrderClose(ticket ,OrderLots() , price ,3, col);
			if( resultClose ){
				resultReturn = true;
				break;
			}else{
				Print("Tong_Zuu >> Count ["+num+"] Select tiecket : "+ticket+" is success. but cannot close order. try again.");
			}
		}else{
			Print("Tong_Zuu >> "+num+" Cannot Select tiecket : "+ticket+" try again");
			if(OrderSelect(ticket , SELECT_BY_TICKET , MODE_HISTORY )){
				Print("Tong_Zuu >> tiecket : "+ticket+" in history clear list");
				resultReturn = true;
				break;
			}
		}
		
	}while(num < 2);
	return resultReturn;
}

void loadHistory(){
	int orderTotal = OrdersTotal();
	Print("TongZuu >> OnInit : "+Symbol()+" Count : "+orderTotal);
	for(int Counter = orderTotal - 1; Counter >= 0; Counter--){
		if(OrderSelect(Counter,SELECT_BY_POS)){
			int ordMagicNum = OrderMagicNumber();
			bool magicDefault = ordMagicNum == MagicNumber;
			if(OrderSymbol() == Symbol()){
				double price = OrderOpenPrice();
				Print("No : ["+Counter+"] Symbol : ["+OrderSymbol()+"] Ticket : ["+OrderTicket()+"] Type : ["+OrderType()+"] Price : ["+price+"]");
				if( OrderType() == OP_BUY ){
					int idxBuy = MathFloor(((qualityBuy - price) / Zonepitch)-1);
					if(idxBuy >= 0 &&  idxBuy < 60){
						if(myTicket[idxBuy].buyC == -1 && (magicDefault || MagicC == ordMagicNum) ){
							myTicket[idxBuy].buyC = OrderTicket();
							Print("Type Buy C : Index[ "+idxBuy+" ][ "+myTicket[idxBuy].buyC+" ][ "+myTicket[idxBuy].buyB+" ][ "+myTicket[idxBuy].buyA+" ][ "+OrderComment()+" ]");
							continue;
						}
						if(myTicket[idxBuy].buyB == -1 && (magicDefault || MagicB == ordMagicNum) ){
							myTicket[idxBuy].buyB = OrderTicket();
							Print("Type Buy B : Index[ "+idxBuy+" ][ "+myTicket[idxBuy].buyC+" ][ "+myTicket[idxBuy].buyB+" ][ "+myTicket[idxBuy].buyA+" ][ "+OrderComment()+" ]");
							continue;
						}
						if(myTicket[idxBuy].buyA == -1  && (magicDefault || MagicA == ordMagicNum) ){
							myTicket[idxBuy].buyA = OrderTicket();
							Print("Type Buy A : Index[ "+idxBuy+" ][ "+myTicket[idxBuy].buyC+" ][ "+myTicket[idxBuy].buyB+" ][ "+myTicket[idxBuy].buyA+" ][ "+OrderComment()+" ]");
							continue;
						}
					}
				}else if(OrderType() == OP_SELL){
					int idxSell = MathFloor(((price - qualitySell) / Zonepitch)-1);
					if( idxSell >= 0 && idxSell < 60 ){
						if(myTicket[idxSell].sellC == -1 && (magicDefault || MagicC == ordMagicNum) ){
							myTicket[idxSell].sellC = OrderTicket();
							Print("Type Sell C : Index[ "+idxSell+" ][ "+myTicket[idxSell].sellC+" ][ "+myTicket[idxSell].sellB+" ][ "+myTicket[idxSell].sellA+" ][ "+OrderComment()+" ]");
							continue;
						}
						if(myTicket[idxSell].sellB == -1 && (magicDefault || MagicB == ordMagicNum) ){
							myTicket[idxSell].sellB = OrderTicket();
							Print("Type Sell B : Index[ "+idxSell+" ][ "+myTicket[idxSell].sellC+" ][ "+myTicket[idxSell].sellB+" ][ "+myTicket[idxSell].sellA+" ][ "+OrderComment()+" ]");
							continue;
						}
						if(myTicket[idxSell].sellA == -1 && (magicDefault || MagicA == ordMagicNum) ){
							myTicket[idxSell].sellA = OrderTicket();
							Print("Type Sell A : Index[ "+idxSell+" ][ "+myTicket[idxSell].sellC+" ][ "+myTicket[idxSell].sellB+" ][ "+myTicket[idxSell].sellA+" ][ "+OrderComment()+" ]");
							continue;
						}
					}
				}
			}else{
				Print("Symbol not match or magic number not match :: Main : ["+Symbol()+"] Order : ["+OrderSymbol()+"] Magic Number : ["+OrderMagicNumber()+"]");
			}
		}
	}
}
