//+------------------------------------------------------------------+
//|                                   Korawit Zone Trading_Draft.mq4 |
//|                                                Korawit Pongsuwan |
//|                       https://www.facebook.com/tomatoze.investor |
//+------------------------------------------------------------------+
#property copyright "Korawit Pongsuwan"
#property link      "https://www.facebook.com/tomatoze.investor"
#property version   "1.00"
#property strict
  //---- input parameters for regular doji
extern bool      FindRegularDoji=true; //false to disable
extern int       MinLengthOfUpTail=3; //candle with upper tail equal or more than this will show up
extern int       MinLengthOfLoTail=3; //candle with lower tail equal or more than this will show up
extern double    MaxLengthOfBody=51.0; //candle with body less or equal with this will show up

extern bool       Dynamiclotsize = true;
extern bool       fiftlot = 1;
extern double Lot = 0.01 ;
bool       SellExtraHedging = 0,BuyExtraHedging = 0;
int BuyExtraHedgingfactor =0,SellExtraHedgingfactor = 0;

double Op,Cl,Hi,Lo,Ti ;
double pt ;
int num = 0;

double UPBB , LOBB ,Sto,EMA100 ;

extern double Zonepitch = 5.00 ;


extern bool CheckOncePerBar = true;
datetime CurrentTimeStamp;
bool NewBar;
int BarShift = 1;
int MagicNumber = 1234;
bool BullSig , BareSig;
int BuyTicketA1,BuyTicketA2,BuyTicketA3,BuyTicketA4,BuyTicketA5,BuyTicketA6,BuyTicketA7,BuyTicketA8,BuyTicketA9,BuyTicketA10;
int BuyTicketA11,BuyTicketA12,BuyTicketA13,BuyTicketA14,BuyTicketA15,BuyTicketA16,BuyTicketA17,BuyTicketA18,BuyTicketA19,BuyTicketA20;
int BuyTicketA21,BuyTicketA22,BuyTicketA23,BuyTicketA24,BuyTicketA25,BuyTicketA26,BuyTicketA27,BuyTicketA28,BuyTicketA29,BuyTicketA30;
int BuyTicketA31,BuyTicketA32,BuyTicketA33,BuyTicketA34,BuyTicketA35,BuyTicketA36,BuyTicketA37,BuyTicketA38,BuyTicketA39,BuyTicketA40;
int BuyTicketA41,BuyTicketA42,BuyTicketA43,BuyTicketA44,BuyTicketA45,BuyTicketA46,BuyTicketA47,BuyTicketA48,BuyTicketA49,BuyTicketA50;
int BuyTicketA51,BuyTicketA52,BuyTicketA53,BuyTicketA54,BuyTicketA55,BuyTicketA56,BuyTicketA57,BuyTicketA58,BuyTicketA59,BuyTicketA60;
int BuyTicketB1,BuyTicketB2,BuyTicketB3,BuyTicketB4,BuyTicketB5,BuyTicketB6,BuyTicketB7,BuyTicketB8,BuyTicketB9,BuyTicketB10;
int BuyTicketB11,BuyTicketB12,BuyTicketB13,BuyTicketB14,BuyTicketB15,BuyTicketB16,BuyTicketB17,BuyTicketB18,BuyTicketB19,BuyTicketB20;
int BuyTicketB21,BuyTicketB22,BuyTicketB23,BuyTicketB24,BuyTicketB25,BuyTicketB26,BuyTicketB27,BuyTicketB28,BuyTicketB29,BuyTicketB30;
int BuyTicketB31,BuyTicketB32,BuyTicketB33,BuyTicketB34,BuyTicketB35,BuyTicketB36,BuyTicketB37,BuyTicketB38,BuyTicketB39,BuyTicketB40;
int BuyTicketB41,BuyTicketB42,BuyTicketB43,BuyTicketB44,BuyTicketB45,BuyTicketB46,BuyTicketB47,BuyTicketB48,BuyTicketB49,BuyTicketB50;
int BuyTicketB51,BuyTicketB52,BuyTicketB53,BuyTicketB54,BuyTicketB55,BuyTicketB56,BuyTicketB57,BuyTicketB58,BuyTicketB59,BuyTicketB60;
int BuyTicketC1,BuyTicketC2,BuyTicketC3,BuyTicketC4,BuyTicketC5,BuyTicketC6,BuyTicketC7,BuyTicketC8,BuyTicketC9,BuyTicketC10;
int BuyTicketC11,BuyTicketC12,BuyTicketC13,BuyTicketC14,BuyTicketC15,BuyTicketC16,BuyTicketC17,BuyTicketC18,BuyTicketC19,BuyTicketC20;
int BuyTicketC21,BuyTicketC22,BuyTicketC23,BuyTicketC24,BuyTicketC25,BuyTicketC26,BuyTicketC27,BuyTicketC28,BuyTicketC29,BuyTicketC30;
int BuyTicketC31,BuyTicketC32,BuyTicketC33,BuyTicketC34,BuyTicketC35,BuyTicketC36,BuyTicketC37,BuyTicketC38,BuyTicketC39,BuyTicketC40;
int BuyTicketC41,BuyTicketC42,BuyTicketC43,BuyTicketC44,BuyTicketC45,BuyTicketC46,BuyTicketC47,BuyTicketC48,BuyTicketC49,BuyTicketC50;
int BuyTicketC51,BuyTicketC52,BuyTicketC53,BuyTicketC54,BuyTicketC55,BuyTicketC56,BuyTicketC57,BuyTicketC58,BuyTicketC59,BuyTicketC60;
int SellTicketA1,SellTicketA2,SellTicketA3,SellTicketA4,SellTicketA5,SellTicketA6,SellTicketA7,SellTicketA8,SellTicketA9,SellTicketA10;
int SellTicketA11,SellTicketA12,SellTicketA13,SellTicketA14,SellTicketA15,SellTicketA16,SellTicketA17,SellTicketA18,SellTicketA19,SellTicketA20;
int SellTicketA21,SellTicketA22,SellTicketA23,SellTicketA24,SellTicketA25,SellTicketA26,SellTicketA27,SellTicketA28,SellTicketA29,SellTicketA30;
int SellTicketA31,SellTicketA32,SellTicketA33,SellTicketA34,SellTicketA35,SellTicketA36,SellTicketA37,SellTicketA38,SellTicketA39,SellTicketA40;
int SellTicketA41,SellTicketA42,SellTicketA43,SellTicketA44,SellTicketA45,SellTicketA46,SellTicketA47,SellTicketA48,SellTicketA49,SellTicketA50;
int SellTicketA51,SellTicketA52,SellTicketA53,SellTicketA54,SellTicketA55,SellTicketA56,SellTicketA57,SellTicketA58,SellTicketA59,SellTicketA60;
int SellTicketB1,SellTicketB2,SellTicketB3,SellTicketB4,SellTicketB5,SellTicketB6,SellTicketB7,SellTicketB8,SellTicketB9,SellTicketB10;
int SellTicketB11,SellTicketB12,SellTicketB13,SellTicketB14,SellTicketB15,SellTicketB16,SellTicketB17,SellTicketB18,SellTicketB19,SellTicketB20;
int SellTicketB21,SellTicketB22,SellTicketB23,SellTicketB24,SellTicketB25,SellTicketB26,SellTicketB27,SellTicketB28,SellTicketB29,SellTicketB30;
int SellTicketB31,SellTicketB32,SellTicketB33,SellTicketB34,SellTicketB35,SellTicketB36,SellTicketB37,SellTicketB38,SellTicketB39,SellTicketB40;
int SellTicketB41,SellTicketB42,SellTicketB43,SellTicketB44,SellTicketB45,SellTicketB46,SellTicketB47,SellTicketB48,SellTicketB49,SellTicketB50;
int SellTicketB51,SellTicketB52,SellTicketB53,SellTicketB54,SellTicketB55,SellTicketB56,SellTicketB57,SellTicketB58,SellTicketB59,SellTicketB60;
int SellTicketC1,SellTicketC2,SellTicketC3,SellTicketC4,SellTicketC5,SellTicketC6,SellTicketC7,SellTicketC8,SellTicketC9,SellTicketC10;
int SellTicketC11,SellTicketC12,SellTicketC13,SellTicketC14,SellTicketC15,SellTicketC16,SellTicketC17,SellTicketC18,SellTicketC19,SellTicketC20;
int SellTicketC21,SellTicketC22,SellTicketC23,SellTicketC24,SellTicketC25,SellTicketC26,SellTicketC27,SellTicketC28,SellTicketC29,SellTicketC30;
int SellTicketC31,SellTicketC32,SellTicketC33,SellTicketC34,SellTicketC35,SellTicketC36,SellTicketC37,SellTicketC38,SellTicketC39,SellTicketC40;
int SellTicketC41,SellTicketC42,SellTicketC43,SellTicketC44,SellTicketC45,SellTicketC46,SellTicketC47,SellTicketC48,SellTicketC49,SellTicketC50;
int SellTicketC51,SellTicketC52,SellTicketC53,SellTicketC54,SellTicketC55,SellTicketC56,SellTicketC57,SellTicketC58,SellTicketC59,SellTicketC60;

double CurrentBuyProfit = 0, CurrentSellProfit = 0;

string Zone = "Zone 0";

 

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
//---
   pt = Point();
   CurrentTimeStamp = Time[0];
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
Hi = iHigh(Symbol(),0,1);
Lo = iLow(Symbol(),0,1);
Op = iOpen(Symbol(),0,1);
Cl = iClose(Symbol(),0,1);
Ti = iTime(Symbol(),0,1);

UPBB = iBands(NULL,0,20,2,0,PRICE_CLOSE,MODE_UPPER,0);
LOBB = iBands(NULL,0,20,2,0,PRICE_CLOSE,MODE_LOWER,0);
Sto = iStochastic(NULL,0,5,3,3,MODE_EMA,0,MODE_MAIN,1);
EMA100 = iMA(NULL,PERIOD_CURRENT,100,0,MODE_EMA,PRICE_WEIGHTED,0);

CurrentBuyProfit = CurrentBuyProfitfn(BuyTicketA1,BuyTicketA2,BuyTicketA3,BuyTicketA4,BuyTicketA5);

if(CheckOncePerBar == true){

   if(CurrentTimeStamp != Time[0]) {
	   CurrentTimeStamp = Time[0];
	   NewBar = true;
   }else NewBar = false;
}else {
	NewBar = true;
	BarShift = 0;
}

BullSig = (Hi-Cl>=MinLengthOfUpTail*pt && Cl-Lo>=MinLengthOfLoTail*pt  && MathAbs(Cl-Op)<=MaxLengthOfBody*pt && NewBar && (Hi+Lo+Lo+Lo+Cl+Cl )/6 < LOBB);
BareSig = (Hi-Cl>=MinLengthOfUpTail*pt && Cl-Lo>=MinLengthOfLoTail*pt  && MathAbs(Cl-Op)<=MaxLengthOfBody*pt && NewBar && (Hi+Hi+Hi+Lo+Cl+Cl )/6 >UPBB);

if(BullSig){
      ObjectCreate(Symbol(),"Text_"+ num,OBJ_TEXT,0,Ti,Lo-100*pt);
      ObjectSetText("Text_"+num,"Doji",7,"Times New Roman",Yellow);
      num++;
}else if(BareSig ){
      ObjectCreate(Symbol(),"Text_"+num,OBJ_TEXT,0,Ti,Hi+100*pt);
      ObjectSetText("Text_"+num,"Doji",7,"Times New Roman",Yellow);
      num++;
}


if(SellTicketA1==0 && 1170 + Zonepitch*2 > Bid  && Bid> 1170 + Zonepitch*1 ){
	SellTicketA1 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA1!=0 &&  Ask < 1170 + Zonepitch*0 ){
	OrderSelect(SellTicketA1,SELECT_BY_TICKET);
	OrderClose(SellTicketA1,OrderLots(),Ask,3,Red);
	SellTicketA1 = 0;
}


if(SellTicketA2==0 && 1170 + Zonepitch*3 > Bid  && Bid> 1170 + Zonepitch*2 ){
	SellTicketA2 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA2!=0 &&  Ask < 1170 + Zonepitch*1 ){
	OrderSelect(SellTicketA2,SELECT_BY_TICKET);
	OrderClose(SellTicketA2,OrderLots(),Ask,3,Red);
	SellTicketA2 = 0;
}

if(SellTicketA3==0 && 1170 + Zonepitch*4 > Bid  && Bid> 1170 + Zonepitch*3 ){
	SellTicketA3 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA3!=0 &&  Ask < 1170 + Zonepitch*2 ){
	OrderSelect(SellTicketA3,SELECT_BY_TICKET);
	OrderClose(SellTicketA3,OrderLots(),Ask,3,Red);
	SellTicketA3 = 0;
}

if(SellTicketA4==0 && 1170 + Zonepitch*5 > Bid  && Bid> 1170 + Zonepitch*4 ){
	SellTicketA4 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA4!=0 &&  Ask < 1170 + Zonepitch*3 ){
	OrderSelect(SellTicketA4,SELECT_BY_TICKET);
	OrderClose(SellTicketA4,OrderLots(),Ask,3,Red);
	SellTicketA4 = 0;
}

if(SellTicketA5==0 && 1170 + Zonepitch*6 > Bid  && Bid> 1170 + Zonepitch*5 ){
	SellTicketA5 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA5!=0 &&  Ask < 1170 + Zonepitch*4 ){
	OrderSelect(SellTicketA5,SELECT_BY_TICKET);
	OrderClose(SellTicketA5,OrderLots(),Ask,3,Red);
	SellTicketA5 = 0;
}

if(SellTicketA6==0 && 1170 + Zonepitch*7 > Bid  && Bid> 1170 + Zonepitch*6 ){
	SellTicketA6 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA6!=0 &&  Ask < 1170 + Zonepitch*5 ){
	OrderSelect(SellTicketA6,SELECT_BY_TICKET);
	OrderClose(SellTicketA6,OrderLots(),Ask,3,Red);
	SellTicketA6 = 0;
}


if(SellTicketA7==0 && 1170 + Zonepitch*8 > Bid  && Bid> 1170 + Zonepitch*7 ){
	SellTicketA7 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA7!=0 &&  Ask < 1170 + Zonepitch*6 ){
	OrderSelect(SellTicketA7,SELECT_BY_TICKET);
	OrderClose(SellTicketA7,OrderLots(),Ask,3,Red);
	SellTicketA7 = 0;
}


if(SellTicketA8==0 && 1170 + Zonepitch*9 > Bid  && Bid> 1170 + Zonepitch*8 ){
	SellTicketA8 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA8!=0 &&  Ask < 1170 + Zonepitch*7 ){
	OrderSelect(SellTicketA8,SELECT_BY_TICKET);
	OrderClose(SellTicketA8,OrderLots(),Ask,3,Red);
	SellTicketA8 = 0;
}


if(SellTicketA9==0 && 1170 + Zonepitch*10 > Bid  && Bid> 1170 + Zonepitch*9 ){
	SellTicketA9 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA9!=0 &&  Ask < 1170 + Zonepitch*8 ){
	OrderSelect(SellTicketA9,SELECT_BY_TICKET);
	OrderClose(SellTicketA9,OrderLots(),Ask,3,Red);
	SellTicketA9 = 0;
}


if(SellTicketA10==0 && 1170 + Zonepitch*11 > Bid  && Bid> 1170 + Zonepitch*10 ){
	SellTicketA10 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA10!=0 &&  Ask < 1170 + Zonepitch*9 ){
	OrderSelect(SellTicketA10,SELECT_BY_TICKET);
	OrderClose(SellTicketA10,OrderLots(),Ask,3,Red);
	SellTicketA10 = 0;
}


if(SellTicketA11==0 && 1170 + Zonepitch*12 > Bid  && Bid> 1170 + Zonepitch*11 ){
	SellTicketA11 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA11!=0 &&  Ask < 1170 + Zonepitch*10 ){
	OrderSelect(SellTicketA11,SELECT_BY_TICKET);
	OrderClose(SellTicketA11,OrderLots(),Ask,3,Red);
	SellTicketA11 = 0;
}

if(SellTicketA12==0 && 1170 + Zonepitch*13 > Bid  && Bid> 1170 + Zonepitch*12 ){
	SellTicketA12 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA12!=0 &&  Ask < 1170 + Zonepitch*11 ){
	OrderSelect(SellTicketA12,SELECT_BY_TICKET);
	OrderClose(SellTicketA12,OrderLots(),Ask,3,Red);
	SellTicketA12 = 0;
}

if(SellTicketA13==0 && 1170 + Zonepitch*14 > Bid  && Bid> 1170 + Zonepitch*13 ){
	SellTicketA13 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA13!=0 &&  Ask < 1170 + Zonepitch*12 ){
	OrderSelect(SellTicketA13,SELECT_BY_TICKET);
	OrderClose(SellTicketA13,OrderLots(),Ask,3,Red);
	SellTicketA13 = 0;
}


if(SellTicketA14==0 && 1170 + Zonepitch*15 > Bid  && Bid> 1170 + Zonepitch*14 ){
	SellTicketA14 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA14!=0 &&  Ask < 1170 + Zonepitch*13 ){
	OrderSelect(SellTicketA14,SELECT_BY_TICKET);
	OrderClose(SellTicketA14,OrderLots(),Ask,3,Red);
	SellTicketA14 = 0;
}


if(SellTicketA15==0 && 1170 + Zonepitch*16 > Bid  && Bid> 1170 + Zonepitch*15 ){
	SellTicketA15 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA15!=0 &&  Ask < 1170 + Zonepitch*14 ){
	OrderSelect(SellTicketA15,SELECT_BY_TICKET);
	OrderClose(SellTicketA15,OrderLots(),Ask,3,Red);
	SellTicketA15 = 0;
}


if(SellTicketA16==0 && 1170 + Zonepitch*17 > Bid  && Bid> 1170 + Zonepitch*16 ){
	SellTicketA16 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA16!=0 &&  Ask < 1170 + Zonepitch*15 ){
	OrderSelect(SellTicketA16,SELECT_BY_TICKET);
	OrderClose(SellTicketA16,OrderLots(),Ask,3,Red);
	SellTicketA16 = 0;
}

if(SellTicketA17==0 && 1170 + Zonepitch*18 > Bid  && Bid> 1170 + Zonepitch*17 ){
	SellTicketA17 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA17!=0 &&  Ask < 1170 + Zonepitch*16 ){
	OrderSelect(SellTicketA17,SELECT_BY_TICKET);
	OrderClose(SellTicketA17,OrderLots(),Ask,3,Red);
	SellTicketA17 = 0;
}


if(SellTicketA18==0 && 1170 + Zonepitch*19 > Bid  && Bid> 1170 + Zonepitch*18 ){
	SellTicketA18 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA18!=0 &&  Ask < 1170 + Zonepitch*17 ){
	OrderSelect(SellTicketA18,SELECT_BY_TICKET);
	OrderClose(SellTicketA18,OrderLots(),Ask,3,Red);
	SellTicketA18 = 0;
}


if(SellTicketA19==0 && 1170 + Zonepitch*20 > Bid  && Bid> 1170 + Zonepitch*19 ){
	SellTicketA19 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA19!=0 &&  Ask < 1170 + Zonepitch*18 ){
	OrderSelect(SellTicketA19,SELECT_BY_TICKET);
	OrderClose(SellTicketA19,OrderLots(),Ask,3,Red);
	SellTicketA19 = 0;
}


if(SellTicketA20==0 && 1170 + Zonepitch*21 > Bid  && Bid> 1170 + Zonepitch*20 ){
	SellTicketA20 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA20!=0 &&  Ask < 1170 + Zonepitch*19 ){
	OrderSelect(SellTicketA20,SELECT_BY_TICKET);
	OrderClose(SellTicketA20,OrderLots(),Ask,3,Red);
	SellTicketA20 = 0;
}

if(SellTicketA21==0 && 1170 + Zonepitch*22 > Bid  && Bid> 1170 + Zonepitch*21 ){
	SellTicketA21 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA21!=0 &&  Ask < 1170 + Zonepitch*20 ){
	OrderSelect(SellTicketA21,SELECT_BY_TICKET);
	OrderClose(SellTicketA21,OrderLots(),Ask,3,Red);
	SellTicketA21 = 0;
}


if(SellTicketA22==0 && 1170 + Zonepitch*23 > Bid  && Bid> 1170 + Zonepitch*22 ){
	SellTicketA22 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA22!=0 &&  Ask < 1170 + Zonepitch*21 ){
	OrderSelect(SellTicketA22,SELECT_BY_TICKET);
	OrderClose(SellTicketA22,OrderLots(),Ask,3,Red);
	SellTicketA22 = 0;
}


if(SellTicketA23==0 && 1170 + Zonepitch*24 > Bid  && Bid> 1170 + Zonepitch*23 ){
	SellTicketA23 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA23!=0 &&  Ask < 1170 + Zonepitch*22 ){
	OrderSelect(SellTicketA23,SELECT_BY_TICKET);
	OrderClose(SellTicketA23,OrderLots(),Ask,3,Red);
	SellTicketA23 = 0;
}


if(SellTicketA24==0 && 1170 + Zonepitch*25 > Bid  && Bid> 1170 + Zonepitch*24 ){
	SellTicketA24 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA24!=0 &&  Ask < 1170 + Zonepitch*23 ){
	OrderSelect(SellTicketA24,SELECT_BY_TICKET);
	OrderClose(SellTicketA24,OrderLots(),Ask,3,Red);
	SellTicketA24 = 0;
}


if(SellTicketA25==0 && 1170 + Zonepitch*26 > Bid  && Bid> 1170 + Zonepitch*25 ){
	SellTicketA25 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA25!=0 &&  Ask < 1170 + Zonepitch*24 ){
	OrderSelect(SellTicketA25,SELECT_BY_TICKET);
	OrderClose(SellTicketA25,OrderLots(),Ask,3,Red);
	SellTicketA25 = 0;
}


if(SellTicketA26==0 && 1170 + Zonepitch*27 > Bid  && Bid> 1170 + Zonepitch*26 ){
	SellTicketA26 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA26!=0 &&  Ask < 1170 + Zonepitch*25 ){
	OrderSelect(SellTicketA26,SELECT_BY_TICKET);
	OrderClose(SellTicketA26,OrderLots(),Ask,3,Red);
	SellTicketA26 = 0;
}


if(SellTicketA27==0 && 1170 + Zonepitch*28 > Bid  && Bid> 1170 + Zonepitch*27 ){
	SellTicketA27 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA27!=0 &&  Ask < 1170 + Zonepitch*26 ){
	OrderSelect(SellTicketA27,SELECT_BY_TICKET);
	OrderClose(SellTicketA27,OrderLots(),Ask,3,Red);
	SellTicketA27 = 0;
}


if(SellTicketA28==0 && 1170 + Zonepitch*29 > Bid  && Bid> 1170 + Zonepitch*28 ){
	SellTicketA28 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA28!=0 &&  Ask < 1170 + Zonepitch*27 ){
	OrderSelect(SellTicketA28,SELECT_BY_TICKET);
	OrderClose(SellTicketA28,OrderLots(),Ask,3,Red);
	SellTicketA28 = 0;
}


if(SellTicketA29==0 && 1170 + Zonepitch*30 > Bid  && Bid> 1170 + Zonepitch*29 ){
	SellTicketA29 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA29!=0 &&  Ask < 1170 + Zonepitch*28 ){
	OrderSelect(SellTicketA29,SELECT_BY_TICKET);
	OrderClose(SellTicketA29,OrderLots(),Ask,3,Red);
	SellTicketA29 = 0;
}


if(SellTicketA30==0 && 1170 + Zonepitch*31 > Bid  && Bid> 1170 + Zonepitch*30 ){
	SellTicketA30 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA30!=0 &&  Ask < 1170 + Zonepitch*29 ){
	OrderSelect(SellTicketA30,SELECT_BY_TICKET);
	OrderClose(SellTicketA30,OrderLots(),Ask,3,Red);
	SellTicketA30 = 0;
}


if(SellTicketA31==0 && 1170 + Zonepitch*32 > Bid  && Bid> 1170 + Zonepitch*31 ){
	SellTicketA31 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA31!=0 &&  Ask < 1170 + Zonepitch*30 ){
	OrderSelect(SellTicketA31,SELECT_BY_TICKET);
	OrderClose(SellTicketA31,OrderLots(),Ask,3,Red);
	SellTicketA31 = 0;
}

if(SellTicketA32==0 && 1170 + Zonepitch*33 > Bid  && Bid> 1170 + Zonepitch*32 ){
	SellTicketA32 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA32!=0 &&  Ask < 1170 + Zonepitch*31 ){
	OrderSelect(SellTicketA32,SELECT_BY_TICKET);
	OrderClose(SellTicketA32,OrderLots(),Ask,3,Red);
	SellTicketA32 = 0;
}

if(SellTicketA33==0 && 1170 + Zonepitch*34 > Bid  && Bid> 1170 + Zonepitch*33 ){
	SellTicketA33 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA33!=0 &&  Ask < 1170 + Zonepitch*32 ){
	OrderSelect(SellTicketA33,SELECT_BY_TICKET);
	OrderClose(SellTicketA33,OrderLots(),Ask,3,Red);
	SellTicketA33 = 0;
}

if(SellTicketA34==0 && 1170 + Zonepitch*35 > Bid  && Bid> 1170 + Zonepitch*34 ){
	SellTicketA34 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA34!=0 &&  Ask < 1170 + Zonepitch*33 ){
	OrderSelect(SellTicketA34,SELECT_BY_TICKET);
	OrderClose(SellTicketA34,OrderLots(),Ask,3,Red);
	SellTicketA34 = 0;
}

if(SellTicketA35==0 && 1170 + Zonepitch*36 > Bid  && Bid> 1170 + Zonepitch*35 ){
	SellTicketA35 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA35!=0 &&  Ask < 1170 + Zonepitch*34 ){
	OrderSelect(SellTicketA35,SELECT_BY_TICKET);
	OrderClose(SellTicketA35,OrderLots(),Ask,3,Red);
	SellTicketA35 = 0;
}


if(SellTicketA36==0 && 1170 + Zonepitch*37 > Bid  && Bid> 1170 + Zonepitch*36 ){
	SellTicketA36 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA36!=0 &&  Ask < 1170 + Zonepitch*35 ){
	OrderSelect(SellTicketA36,SELECT_BY_TICKET);
	OrderClose(SellTicketA36,OrderLots(),Ask,3,Red);
	SellTicketA36 = 0;
}

if(SellTicketA37==0 && 1170 + Zonepitch*38 > Bid  && Bid> 1170 + Zonepitch*37 ){
	SellTicketA37 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA37!=0 &&  Ask < 1170 + Zonepitch*36 ){
	OrderSelect(SellTicketA37,SELECT_BY_TICKET);
	OrderClose(SellTicketA37,OrderLots(),Ask,3,Red);
	SellTicketA37 = 0;
}

if(SellTicketA38==0 && 1170 + Zonepitch*39 > Bid  && Bid> 1170 + Zonepitch*38 ){
	SellTicketA38 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA38!=0 &&  Ask < 1170 + Zonepitch*37 ){
	OrderSelect(SellTicketA38,SELECT_BY_TICKET);
	OrderClose(SellTicketA38,OrderLots(),Ask,3,Red);
	SellTicketA38 = 0;
}


if(SellTicketA39==0 && 1170 + Zonepitch*40 > Bid  && Bid> 1170 + Zonepitch*39 ){
	SellTicketA39 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA39!=0 &&  Ask < 1170 + Zonepitch*38 ){
	OrderSelect(SellTicketA39,SELECT_BY_TICKET);
	OrderClose(SellTicketA39,OrderLots(),Ask,3,Red);
	SellTicketA39 = 0;
}

if(SellTicketA40==0 && 1170 + Zonepitch*41 > Bid  && Bid> 1170 + Zonepitch*40 ){
	SellTicketA40 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA40!=0 &&  Ask < 1170 + Zonepitch*39 ){
	OrderSelect(SellTicketA40,SELECT_BY_TICKET);
	OrderClose(SellTicketA40,OrderLots(),Ask,3,Red);
	SellTicketA40 = 0;
}


if(SellTicketA41==0 && 1170 + Zonepitch*42 > Bid  && Bid> 1170 + Zonepitch*41 ){
	SellTicketA41 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA41!=0 &&  Ask < 1170 + Zonepitch*40 ){
	OrderSelect(SellTicketA41,SELECT_BY_TICKET);
	OrderClose(SellTicketA41,OrderLots(),Ask,3,Red);
	SellTicketA41 = 0;
}


if(SellTicketA42==0 && 1170 + Zonepitch*43 > Bid  && Bid> 1170 + Zonepitch*42 ){
	SellTicketA42 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA42!=0 &&  Ask < 1170 + Zonepitch*41 ){
	OrderSelect(SellTicketA42,SELECT_BY_TICKET);
	OrderClose(SellTicketA42,OrderLots(),Ask,3,Red);
	SellTicketA42 = 0;
}

if(SellTicketA43==0 && 1170 + Zonepitch*44 > Bid  && Bid> 1170 + Zonepitch*43 ){
	SellTicketA43 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA43!=0 &&  Ask < 1170 + Zonepitch*42 ){
	OrderSelect(SellTicketA43,SELECT_BY_TICKET);
	OrderClose(SellTicketA43,OrderLots(),Ask,3,Red);
	SellTicketA43 = 0;
}


if(SellTicketA44==0 && 1170 + Zonepitch*45 > Bid  && Bid> 1170 + Zonepitch*44 ){
	SellTicketA44 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA44!=0 &&  Ask < 1170 + Zonepitch*43 ){
	OrderSelect(SellTicketA44,SELECT_BY_TICKET);
	OrderClose(SellTicketA44,OrderLots(),Ask,3,Red);
	SellTicketA44 = 0;
}

if(SellTicketA45==0 && 1170 + Zonepitch*46 > Bid  && Bid> 1170 + Zonepitch*45 ){
	SellTicketA45 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA45!=0 &&  Ask < 1170 + Zonepitch*44 ){
	OrderSelect(SellTicketA45,SELECT_BY_TICKET);
	OrderClose(SellTicketA45,OrderLots(),Ask,3,Red);
	SellTicketA45 = 0;
}


if(SellTicketA46==0 && 1170 + Zonepitch*47 > Bid  && Bid> 1170 + Zonepitch*46 ){
	SellTicketA46 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA46!=0 &&  Ask < 1170 + Zonepitch*45 ){
	OrderSelect(SellTicketA46,SELECT_BY_TICKET);
	OrderClose(SellTicketA46,OrderLots(),Ask,3,Red);
	SellTicketA46 = 0;
}

if(SellTicketA47==0 && 1170 + Zonepitch*48 > Bid  && Bid> 1170 + Zonepitch*47 ){
	SellTicketA47 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA47!=0 &&  Ask < 1170 + Zonepitch*46 ){
	OrderSelect(SellTicketA47,SELECT_BY_TICKET);
	OrderClose(SellTicketA47,OrderLots(),Ask,3,Red);
	SellTicketA47 = 0;
}

if(SellTicketA48==0 && 1170 + Zonepitch*49 > Bid  && Bid> 1170 + Zonepitch*48 ){
	SellTicketA48 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA48!=0 &&  Ask < 1170 + Zonepitch*47 ){
	OrderSelect(SellTicketA48,SELECT_BY_TICKET);
	OrderClose(SellTicketA48,OrderLots(),Ask,3,Red);
	SellTicketA48 = 0;
}


if(SellTicketA49==0 && 1170 + Zonepitch*50 > Bid  && Bid> 1170 + Zonepitch*49 ){
	SellTicketA49 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA49!=0 &&  Ask < 1170 + Zonepitch*48 ){
	OrderSelect(SellTicketA49,SELECT_BY_TICKET);
	OrderClose(SellTicketA49,OrderLots(),Ask,3,Red);
	SellTicketA49 = 0;
}


if(SellTicketA50==0 && 1170 + Zonepitch*51 > Bid  && Bid> 1170 + Zonepitch*50 ){
	SellTicketA50 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA50!=0 &&  Ask < 1170 + Zonepitch*49 ){
	OrderSelect(SellTicketA50,SELECT_BY_TICKET);
	OrderClose(SellTicketA50,OrderLots(),Ask,3,Red);
	SellTicketA50 = 0;
}

if(SellTicketA51==0 && 1170 + Zonepitch*52 > Bid  && Bid> 1170 + Zonepitch*51 ){
	SellTicketA51 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA51!=0 &&  Ask < 1170 + Zonepitch*50 ){
	OrderSelect(SellTicketA51,SELECT_BY_TICKET);
	OrderClose(SellTicketA51,OrderLots(),Ask,3,Red);
	SellTicketA51 = 0;
}

if(SellTicketA52==0 && 1170 + Zonepitch*53 > Bid  && Bid> 1170 + Zonepitch*52 ){
	SellTicketA52 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA52!=0 &&  Ask < 1170 + Zonepitch*51 ){
	OrderSelect(SellTicketA52,SELECT_BY_TICKET);
	OrderClose(SellTicketA52,OrderLots(),Ask,3,Red);
	SellTicketA52 = 0;
}

if(SellTicketA53==0 && 1170 + Zonepitch*54 > Bid  && Bid> 1170 + Zonepitch*53 ){
	SellTicketA53 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA53!=0 &&  Ask < 1170 + Zonepitch*52 ){
	OrderSelect(SellTicketA53,SELECT_BY_TICKET);
	OrderClose(SellTicketA53,OrderLots(),Ask,3,Red);
	SellTicketA53 = 0;
}

if(SellTicketA54==0 && 1170 + Zonepitch*55 > Bid  && Bid> 1170 + Zonepitch*54 ){
	SellTicketA54 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA54!=0 &&  Ask < 1170 + Zonepitch*53 ){
	OrderSelect(SellTicketA54,SELECT_BY_TICKET);
	OrderClose(SellTicketA54,OrderLots(),Ask,3,Red);
	SellTicketA54 = 0;
}

if(SellTicketA55==0 && 1170 + Zonepitch*56 > Bid  && Bid> 1170 + Zonepitch*55 ){
	SellTicketA55 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA55!=0 &&  Ask < 1170 + Zonepitch*54 ){
	OrderSelect(SellTicketA55,SELECT_BY_TICKET);
	OrderClose(SellTicketA55,OrderLots(),Ask,3,Red);
	SellTicketA55 = 0;
}

if(SellTicketA56==0 && 1170 + Zonepitch*57 > Bid  && Bid> 1170 + Zonepitch*56 ){
	SellTicketA56 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA56!=0 &&  Ask < 1170 + Zonepitch*55 ){
	OrderSelect(SellTicketA56,SELECT_BY_TICKET);
	OrderClose(SellTicketA56,OrderLots(),Ask,3,Red);
	SellTicketA56 = 0;
}

if(SellTicketA57==0 && 1170 + Zonepitch*58 > Bid  && Bid> 1170 + Zonepitch*57 ){
	SellTicketA57 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA57!=0 &&  Ask < 1170 + Zonepitch*56 ){
	OrderSelect(SellTicketA57,SELECT_BY_TICKET);
	OrderClose(SellTicketA57,OrderLots(),Ask,3,Red);
	SellTicketA57 = 0;
}

if(SellTicketA58==0 && 1170 + Zonepitch*59 > Bid  && Bid> 1170 + Zonepitch*58 ){
	SellTicketA58 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA58!=0 &&  Ask < 1170 + Zonepitch*57 ){
	OrderSelect(SellTicketA58,SELECT_BY_TICKET);
	OrderClose(SellTicketA58,OrderLots(),Ask,3,Red);
	SellTicketA58 = 0;
}

if(SellTicketA59==0 && 1170 + Zonepitch*60 > Bid  && Bid> 1170 + Zonepitch*59 ){
	SellTicketA59 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA59!=0 &&  Ask < 1170 + Zonepitch*58 ){
	OrderSelect(SellTicketA59,SELECT_BY_TICKET);
	OrderClose(SellTicketA59,OrderLots(),Ask,3,Red);
	SellTicketA59 = 0;
}

if(SellTicketA60==0 && 1170 + Zonepitch*61 > Bid  && Bid> 1170 + Zonepitch*60 ){
	SellTicketA60 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketA60!=0 &&  Ask < 1170 + Zonepitch*59 ){
	OrderSelect(SellTicketA60,SELECT_BY_TICKET);
	OrderClose(SellTicketA60,OrderLots(),Ask,3,Red);
	SellTicketA60 = 0;
}

if(SellTicketB1==0 && 1170 + Zonepitch*2 > Bid  && Bid> 1170 + Zonepitch*1 ){
	SellTicketB1 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB1!=0 &&  Ask < 1170 + Zonepitch*0.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB1,SELECT_BY_TICKET);
	OrderClose(SellTicketB1,OrderLots(),Ask,3,Red);
	SellTicketB1 = 0;
}

if(SellTicketB2==0 && 1170 + Zonepitch*3 > Bid  && Bid> 1170 + Zonepitch*2 ){
	SellTicketB2 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB2!=0 &&  Ask < 1170 + Zonepitch*1.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB2,SELECT_BY_TICKET);
	OrderClose(SellTicketB2,OrderLots(),Ask,3,Red);
	SellTicketB2 = 0;
}

if(SellTicketB3==0 && 1170 + Zonepitch*4 > Bid  && Bid> 1170 + Zonepitch*3 ){
	SellTicketB3 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB3!=0 &&  Ask < 1170 + Zonepitch*2.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB3,SELECT_BY_TICKET);
	OrderClose(SellTicketB3,OrderLots(),Ask,3,Red);
	SellTicketB3 = 0;
}

if(SellTicketB4==0 && 1170 + Zonepitch*5 > Bid  && Bid> 1170 + Zonepitch*4 ){
	SellTicketB4 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB4!=0 &&  Ask < 1170 + Zonepitch*3.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB4,SELECT_BY_TICKET);
	OrderClose(SellTicketB4,OrderLots(),Ask,3,Red);
	SellTicketB4 = 0;
}

if(SellTicketB5==0 && 1170 + Zonepitch*6 > Bid  && Bid> 1170 + Zonepitch*5 ){
	SellTicketB5 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB5!=0 &&  Ask < 1170 + Zonepitch*4.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB5,SELECT_BY_TICKET);
	OrderClose(SellTicketB5,OrderLots(),Ask,3,Red);
	SellTicketB5 = 0;
}

if(SellTicketB6==0 && 1170 + Zonepitch*7 > Bid  && Bid> 1170 + Zonepitch*6 ){
	SellTicketB6 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB6!=0 &&  Ask < 1170 + Zonepitch*5.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB6,SELECT_BY_TICKET);
	OrderClose(SellTicketB6,OrderLots(),Ask,3,Red);
	SellTicketB6 = 0;
}

if(SellTicketB7==0 && 1170 + Zonepitch*8 > Bid  && Bid> 1170 + Zonepitch*7 ){
	SellTicketB7 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB7!=0 &&  Ask < 1170 + Zonepitch*6.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB7,SELECT_BY_TICKET);
	OrderClose(SellTicketB7,OrderLots(),Ask,3,Red);
	SellTicketB7 = 0;
}

if(SellTicketB8==0 && 1170 + Zonepitch*9 > Bid  && Bid> 1170 + Zonepitch*8 ){
	SellTicketB8 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB8!=0 &&  Ask < 1170 + Zonepitch*7.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB8,SELECT_BY_TICKET);
	OrderClose(SellTicketB8,OrderLots(),Ask,3,Red);
	SellTicketB8 = 0;
}


if(SellTicketB9==0 && 1170 + Zonepitch*10 > Bid  && Bid> 1170 + Zonepitch*9 ){
	SellTicketB9 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB9!=0 &&  Ask < 1170 + Zonepitch*8.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB9,SELECT_BY_TICKET);
	OrderClose(SellTicketB9,OrderLots(),Ask,3,Red);
	SellTicketB9 = 0;
}

if(SellTicketB10==0 && 1170 + Zonepitch*11 > Bid  && Bid> 1170 + Zonepitch*10 ){
	SellTicketB10 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB10!=0 &&  Ask < 1170 + Zonepitch*9.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB10,SELECT_BY_TICKET);
	OrderClose(SellTicketB10,OrderLots(),Ask,3,Red);
	SellTicketB10 = 0;
}

if(SellTicketB11==0 && 1170 + Zonepitch*12 > Bid  && Bid> 1170 + Zonepitch*11 ){
	SellTicketB11 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB11!=0 &&  Ask < 1170 + Zonepitch*10.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB11,SELECT_BY_TICKET);
	OrderClose(SellTicketB11,OrderLots(),Ask,3,Red);
	SellTicketB11 = 0;
}

if(SellTicketB12==0 && 1170 + Zonepitch*13 > Bid  && Bid> 1170 + Zonepitch*12 ){
	SellTicketB12 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB12!=0 &&  Ask < 1170 + Zonepitch*11.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB12,SELECT_BY_TICKET);
	OrderClose(SellTicketB12,OrderLots(),Ask,3,Red);
	SellTicketB12 = 0;
}

if(SellTicketB13==0 && 1170 + Zonepitch*14 > Bid  && Bid> 1170 + Zonepitch*13 ){
	SellTicketB13 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB13!=0 &&  Ask < 1170 + Zonepitch*12.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB13,SELECT_BY_TICKET);
	OrderClose(SellTicketB13,OrderLots(),Ask,3,Red);
	SellTicketB13 = 0;
}

if(SellTicketB14==0 && 1170 + Zonepitch*15 > Bid  && Bid> 1170 + Zonepitch*14 ){
	SellTicketB14 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB14!=0 &&  Ask < 1170 + Zonepitch*13.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB14,SELECT_BY_TICKET);
	OrderClose(SellTicketB14,OrderLots(),Ask,3,Red);
	SellTicketB14 = 0;
}

if(SellTicketB15==0 && 1170 + Zonepitch*16 > Bid  && Bid> 1170 + Zonepitch*15 ){
	SellTicketB15 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB15!=0 &&  Ask < 1170 + Zonepitch*14.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB15,SELECT_BY_TICKET);
	OrderClose(SellTicketB15,OrderLots(),Ask,3,Red);
	SellTicketB15 = 0;
}

if(SellTicketB16==0 && 1170 + Zonepitch*17 > Bid  && Bid> 1170 + Zonepitch*16 ){
	SellTicketB16 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB16!=0 &&  Ask < 1170 + Zonepitch*15.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB16,SELECT_BY_TICKET);
	OrderClose(SellTicketB16,OrderLots(),Ask,3,Red);
	SellTicketB16 = 0;
}

if(SellTicketB17==0 && 1170 + Zonepitch*18 > Bid  && Bid> 1170 + Zonepitch*17 ){
	SellTicketB17 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB17!=0 &&  Ask < 1170 + Zonepitch*16.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB17,SELECT_BY_TICKET);
	OrderClose(SellTicketB17,OrderLots(),Ask,3,Red);
	SellTicketB17 = 0;
}

if(SellTicketB18==0 && 1170 + Zonepitch*19 > Bid  && Bid> 1170 + Zonepitch*18 ){
	SellTicketB18 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB18!=0 &&  Ask < 1170 + Zonepitch*17.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB18,SELECT_BY_TICKET);
	OrderClose(SellTicketB18,OrderLots(),Ask,3,Red);
	SellTicketB18 = 0;
}

if(SellTicketB19==0 && 1170 + Zonepitch*20 > Bid  && Bid> 1170 + Zonepitch*19 ){
	SellTicketB19 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB19!=0 &&  Ask < 1170 + Zonepitch*18.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB19,SELECT_BY_TICKET);
	OrderClose(SellTicketB19,OrderLots(),Ask,3,Red);
	SellTicketB19 = 0;
}

if(SellTicketB20==0 && 1170 + Zonepitch*21 > Bid  && Bid> 1170 + Zonepitch*20 ){
	SellTicketB20 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB20!=0 &&  Ask < 1170 + Zonepitch*19.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB20,SELECT_BY_TICKET);
	OrderClose(SellTicketB20,OrderLots(),Ask,3,Red);
	SellTicketB20 = 0;
}

if(SellTicketB21==0 && 1170 + Zonepitch*22 > Bid  && Bid> 1170 + Zonepitch*21 ){
	SellTicketB21 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB21!=0 &&  Ask < 1170 + Zonepitch*20.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB21,SELECT_BY_TICKET);
	OrderClose(SellTicketB21,OrderLots(),Ask,3,Red);
	SellTicketB21 = 0;
}

if(SellTicketB22==0 && 1170 + Zonepitch*23 > Bid  && Bid> 1170 + Zonepitch*22 ){
	SellTicketB22 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB22!=0 &&  Ask < 1170 + Zonepitch*21.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB22,SELECT_BY_TICKET);
	OrderClose(SellTicketB22,OrderLots(),Ask,3,Red);
	SellTicketB22 = 0;
}

if(SellTicketB23==0 && 1170 + Zonepitch*24 > Bid  && Bid> 1170 + Zonepitch*23 ){
	SellTicketB23 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB23!=0 &&  Ask < 1170 + Zonepitch*22.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB23,SELECT_BY_TICKET);
	OrderClose(SellTicketB23,OrderLots(),Ask,3,Red);
	SellTicketB23 = 0;
}

if(SellTicketB24==0 && 1170 + Zonepitch*25 > Bid  && Bid> 1170 + Zonepitch*24 ){
	SellTicketB24 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB24!=0 &&  Ask < 1170 + Zonepitch*23.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB24,SELECT_BY_TICKET);
	OrderClose(SellTicketB24,OrderLots(),Ask,3,Red);
	SellTicketB24 = 0;
}

if(SellTicketB25==0 && 1170 + Zonepitch*26 > Bid  && Bid> 1170 + Zonepitch*25 ){
	SellTicketB25 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB25!=0 &&  Ask < 1170 + Zonepitch*24.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB25,SELECT_BY_TICKET);
	OrderClose(SellTicketB25,OrderLots(),Ask,3,Red);
	SellTicketB25 = 0;
}

if(SellTicketB26==0 && 1170 + Zonepitch*27 > Bid  && Bid> 1170 + Zonepitch*26 ){
	SellTicketB26 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB26!=0 &&  Ask < 1170 + Zonepitch*25.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB26,SELECT_BY_TICKET);
	OrderClose(SellTicketB26,OrderLots(),Ask,3,Red);
	SellTicketB26 = 0;
}

if(SellTicketB27==0 && 1170 + Zonepitch*28 > Bid  && Bid> 1170 + Zonepitch*27 ){
	SellTicketB27 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB27!=0 &&  Ask < 1170 + Zonepitch*26.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB27,SELECT_BY_TICKET);
	OrderClose(SellTicketB27,OrderLots(),Ask,3,Red);
	SellTicketB27 = 0;
}

if(SellTicketB28==0 && 1170 + Zonepitch*29 > Bid  && Bid> 1170 + Zonepitch*28 ){
	SellTicketB28 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB28!=0 &&  Ask < 1170 + Zonepitch*27.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB28,SELECT_BY_TICKET);
	OrderClose(SellTicketB28,OrderLots(),Ask,3,Red);
	SellTicketB28 = 0;
}

if(SellTicketB29==0 && 1170 + Zonepitch*30 > Bid  && Bid> 1170 + Zonepitch*29 ){
	SellTicketB29 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB29!=0 &&  Ask < 1170 + Zonepitch*28.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB29,SELECT_BY_TICKET);
	OrderClose(SellTicketB29,OrderLots(),Ask,3,Red);
	SellTicketB29 = 0;
}

if(SellTicketB30==0 && 1170 + Zonepitch*31 > Bid  && Bid> 1170 + Zonepitch*30 ){
	SellTicketB30 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB30!=0 &&  Ask < 1170 + Zonepitch*29.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB30,SELECT_BY_TICKET);
	OrderClose(SellTicketB30,OrderLots(),Ask,3,Red);
	SellTicketB30 = 0;
}

if(SellTicketB31==0 && 1170 + Zonepitch*32 > Bid  && Bid> 1170 + Zonepitch*31 ){
	SellTicketB31 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB31!=0 &&  Ask < 1170 + Zonepitch*30.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB31,SELECT_BY_TICKET);
	OrderClose(SellTicketB31,OrderLots(),Ask,3,Red);
	SellTicketB31 = 0;
}

if(SellTicketB32==0 && 1170 + Zonepitch*33 > Bid  && Bid> 1170 + Zonepitch*32 ){
	SellTicketB32 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB32!=0 &&  Ask < 1170 + Zonepitch*31.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB32,SELECT_BY_TICKET);
	OrderClose(SellTicketB32,OrderLots(),Ask,3,Red);
	SellTicketB32 = 0;
}

if(SellTicketB33==0 && 1170 + Zonepitch*34 > Bid  && Bid> 1170 + Zonepitch*33 ){
	SellTicketB33 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB33!=0 &&  Ask < 1170 + Zonepitch*32.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB33,SELECT_BY_TICKET);
	OrderClose(SellTicketB33,OrderLots(),Ask,3,Red);
	SellTicketB33 = 0;
}

if(SellTicketB34==0 && 1170 + Zonepitch*35 > Bid  && Bid> 1170 + Zonepitch*34 ){
	SellTicketB34 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB34!=0 &&  Ask < 1170 + Zonepitch*33.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB34,SELECT_BY_TICKET);
	OrderClose(SellTicketB34,OrderLots(),Ask,3,Red);
	SellTicketB34 = 0;
}

if(SellTicketB35==0 && 1170 + Zonepitch*36 > Bid  && Bid> 1170 + Zonepitch*35 ){
	SellTicketB35 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB35!=0 &&  Ask < 1170 + Zonepitch*34.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB35,SELECT_BY_TICKET);
	OrderClose(SellTicketB35,OrderLots(),Ask,3,Red);
	SellTicketB35 = 0;
}

if(SellTicketB36==0 && 1170 + Zonepitch*37 > Bid  && Bid> 1170 + Zonepitch*36 ){
	SellTicketB36 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB36!=0 &&  Ask < 1170 + Zonepitch*35.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB36,SELECT_BY_TICKET);
	OrderClose(SellTicketB36,OrderLots(),Ask,3,Red);
	SellTicketB36 = 0;
}

if(SellTicketB37==0 && 1170 + Zonepitch*38 > Bid  && Bid> 1170 + Zonepitch*37 ){
	SellTicketB37 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB37!=0 &&  Ask < 1170 + Zonepitch*36.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB37,SELECT_BY_TICKET);
	OrderClose(SellTicketB37,OrderLots(),Ask,3,Red);
	SellTicketB37 = 0;
}

if(SellTicketB38==0 && 1170 + Zonepitch*39 > Bid  && Bid> 1170 + Zonepitch*38 ){
	SellTicketB38 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB38!=0 &&  Ask < 1170 + Zonepitch*37.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB38,SELECT_BY_TICKET);
	OrderClose(SellTicketB38,OrderLots(),Ask,3,Red);
	SellTicketB38 = 0;
}

if(SellTicketB39==0 && 1170 + Zonepitch*40 > Bid  && Bid> 1170 + Zonepitch*39 ){
	SellTicketB39 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB39!=0 &&  Ask < 1170 + Zonepitch*38.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB39,SELECT_BY_TICKET);
	OrderClose(SellTicketB39,OrderLots(),Ask,3,Red);
	SellTicketB39 = 0;
}

if(SellTicketB40==0 && 1170 + Zonepitch*41 > Bid  && Bid> 1170 + Zonepitch*40 ){
	SellTicketB40 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB40!=0 &&  Ask < 1170 + Zonepitch*39.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB40,SELECT_BY_TICKET);
	OrderClose(SellTicketB40,OrderLots(),Ask,3,Red);
	SellTicketB40 = 0;
}

if(SellTicketB41==0 && 1170 + Zonepitch*42 > Bid  && Bid> 1170 + Zonepitch*41 ){
	SellTicketB41 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB41!=0 &&  Ask < 1170 + Zonepitch*40.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB41,SELECT_BY_TICKET);
	OrderClose(SellTicketB41,OrderLots(),Ask,3,Red);
	SellTicketB41 = 0;
}

if(SellTicketB42==0 && 1170 + Zonepitch*43 > Bid  && Bid> 1170 + Zonepitch*42 ){
	SellTicketB42 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB42!=0 &&  Ask < 1170 + Zonepitch*41.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB42,SELECT_BY_TICKET);
	OrderClose(SellTicketB42,OrderLots(),Ask,3,Red);
	SellTicketB42 = 0;
}

if(SellTicketB43==0 && 1170 + Zonepitch*44 > Bid  && Bid> 1170 + Zonepitch*43 ){
	SellTicketB43 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB43!=0 &&  Ask < 1170 + Zonepitch*42.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB43,SELECT_BY_TICKET);
	OrderClose(SellTicketB43,OrderLots(),Ask,3,Red);
	SellTicketB43 = 0;
}

if(SellTicketB44==0 && 1170 + Zonepitch*45 > Bid  && Bid> 1170 + Zonepitch*44 ){
	SellTicketB44 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB44!=0 &&  Ask < 1170 + Zonepitch*43.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB44,SELECT_BY_TICKET);
	OrderClose(SellTicketB44,OrderLots(),Ask,3,Red);
	SellTicketB44 = 0;
}

if(SellTicketB45==0 && 1170 + Zonepitch*46 > Bid  && Bid> 1170 + Zonepitch*45 ){
	SellTicketB45 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB45!=0 &&  Ask < 1170 + Zonepitch*44.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB45,SELECT_BY_TICKET);
	OrderClose(SellTicketB45,OrderLots(),Ask,3,Red);
	SellTicketB45 = 0;
}

if(SellTicketB46==0 && 1170 + Zonepitch*47 > Bid  && Bid> 1170 + Zonepitch*46 ){
	SellTicketB46 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB46!=0 &&  Ask < 1170 + Zonepitch*45.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB46,SELECT_BY_TICKET);
	OrderClose(SellTicketB46,OrderLots(),Ask,3,Red);
	SellTicketB46 = 0;
}

if(SellTicketB47==0 && 1170 + Zonepitch*48 > Bid  && Bid> 1170 + Zonepitch*47 ){
	SellTicketB47 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB47!=0 &&  Ask < 1170 + Zonepitch*46.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB47,SELECT_BY_TICKET);
	OrderClose(SellTicketB47,OrderLots(),Ask,3,Red);
	SellTicketB47 = 0;
}

if(SellTicketB48==0 && 1170 + Zonepitch*49 > Bid  && Bid> 1170 + Zonepitch*48 ){
	SellTicketB48 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB48!=0 &&  Ask < 1170 + Zonepitch*47.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB48,SELECT_BY_TICKET);
	OrderClose(SellTicketB48,OrderLots(),Ask,3,Red);
	SellTicketB48 = 0;
}

if(SellTicketB49==0 && 1170 + Zonepitch*50 > Bid  && Bid> 1170 + Zonepitch*49 ){
	SellTicketB49 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB49!=0 &&  Ask < 1170 + Zonepitch*48.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB49,SELECT_BY_TICKET);
	OrderClose(SellTicketB49,OrderLots(),Ask,3,Red);
	SellTicketB49 = 0;
}

if(SellTicketB50==0 && 1170 + Zonepitch*51 > Bid  && Bid> 1170 + Zonepitch*50 ){
	SellTicketB50 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB50!=0 &&  Ask < 1170 + Zonepitch*49.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB50,SELECT_BY_TICKET);
	OrderClose(SellTicketB50,OrderLots(),Ask,3,Red);
	SellTicketB50 = 0;
}

if(SellTicketB51==0 && 1170 + Zonepitch*52 > Bid  && Bid> 1170 + Zonepitch*51 ){
	SellTicketB51 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB51!=0 &&  Ask < 1170 + Zonepitch*50.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB51,SELECT_BY_TICKET);
	OrderClose(SellTicketB51,OrderLots(),Ask,3,Red);
	SellTicketB51 = 0;
}

if(SellTicketB52==0 && 1170 + Zonepitch*53 > Bid  && Bid> 1170 + Zonepitch*52 ){
	SellTicketB52 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB52!=0 &&  Ask < 1170 + Zonepitch*51.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB52,SELECT_BY_TICKET);
	OrderClose(SellTicketB52,OrderLots(),Ask,3,Red);
	SellTicketB52 = 0;
}

if(SellTicketB53==0 && 1170 + Zonepitch*54 > Bid  && Bid> 1170 + Zonepitch*53 ){
	SellTicketB53 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB53!=0 &&  Ask < 1170 + Zonepitch*52.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB53,SELECT_BY_TICKET);
	OrderClose(SellTicketB53,OrderLots(),Ask,3,Red);
	SellTicketB53 = 0;
}

if(SellTicketB54==0 && 1170 + Zonepitch*55 > Bid  && Bid> 1170 + Zonepitch*54 ){
	SellTicketB54 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB54!=0 &&  Ask < 1170 + Zonepitch*53.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB54,SELECT_BY_TICKET);
	OrderClose(SellTicketB54,OrderLots(),Ask,3,Red);
	SellTicketB54 = 0;
}

if(SellTicketB55==0 && 1170 + Zonepitch*56 > Bid  && Bid> 1170 + Zonepitch*55 ){
	SellTicketB55 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB55!=0 &&  Ask < 1170 + Zonepitch*54.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB55,SELECT_BY_TICKET);
	OrderClose(SellTicketB55,OrderLots(),Ask,3,Red);
	SellTicketB55 = 0;
}

if(SellTicketB56==0 && 1170 + Zonepitch*57 > Bid  && Bid> 1170 + Zonepitch*56 ){
	SellTicketB56 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB56!=0 &&  Ask < 1170 + Zonepitch*55.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB56,SELECT_BY_TICKET);
	OrderClose(SellTicketB56,OrderLots(),Ask,3,Red);
	SellTicketB56 = 0;
}

if(SellTicketB57==0 && 1170 + Zonepitch*58 > Bid  && Bid> 1170 + Zonepitch*57 ){
	SellTicketB57 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB57!=0 &&  Ask < 1170 + Zonepitch*56.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB57,SELECT_BY_TICKET);
	OrderClose(SellTicketB57,OrderLots(),Ask,3,Red);
	SellTicketB57 = 0;
}

if(SellTicketB58==0 && 1170 + Zonepitch*59 > Bid  && Bid> 1170 + Zonepitch*58 ){
	SellTicketB58 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB58!=0 &&  Ask < 1170 + Zonepitch*57.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB58,SELECT_BY_TICKET);
	OrderClose(SellTicketB58,OrderLots(),Ask,3,Red);
	SellTicketB58 = 0;
}

if(SellTicketB59==0 && 1170 + Zonepitch*60 > Bid  && Bid> 1170 + Zonepitch*59 ){
	SellTicketB59 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB59!=0 &&  Ask < 1170 + Zonepitch*58.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB59,SELECT_BY_TICKET);
	OrderClose(SellTicketB59,OrderLots(),Ask,3,Red);
	SellTicketB59 = 0;
}

if(SellTicketB60==0 && 1170 + Zonepitch*61 > Bid  && Bid> 1170 + Zonepitch*60 ){
	SellTicketB60 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketB60!=0 &&  Ask < 1170 + Zonepitch*59.8  && Ask < EMA100 ){
	OrderSelect(SellTicketB60,SELECT_BY_TICKET);
	OrderClose(SellTicketB60,OrderLots(),Ask,3,Red);
	SellTicketB60 = 0;
}

if(SellTicketC1==0 && 1170 + Zonepitch*2 > Bid  && Bid> 1170 + Zonepitch*1 ){
	SellTicketC1 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC1!=0 &&  Ask < 1170 + Zonepitch*0.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC1,SELECT_BY_TICKET);
	OrderClose(SellTicketC1,OrderLots(),Ask,3,Red);
	SellTicketC1 = 0;
}

if(SellTicketC2==0 && 1170 + Zonepitch*3 > Bid  && Bid> 1170 + Zonepitch*2 ){
	SellTicketC2 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC2!=0 &&  Ask < 1170 + Zonepitch*1.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC2,SELECT_BY_TICKET);
	OrderClose(SellTicketC2,OrderLots(),Ask,3,Red);
	SellTicketC2 = 0;
}

if(SellTicketC3==0 && 1170 + Zonepitch*4 > Bid  && Bid> 1170 + Zonepitch*3 ){
	SellTicketC3 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC3!=0 &&  Ask < 1170 + Zonepitch*2.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC3,SELECT_BY_TICKET);
	OrderClose(SellTicketC3,OrderLots(),Ask,3,Red);
	SellTicketC3 = 0;
}

if(SellTicketC4==0 && 1170 + Zonepitch*5 > Bid  && Bid> 1170 + Zonepitch*4 ){
	SellTicketC4 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC4!=0 &&  Ask < 1170 + Zonepitch*3.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC4,SELECT_BY_TICKET);
	OrderClose(SellTicketC4,OrderLots(),Ask,3,Red);
	SellTicketC4 = 0;
}

if(SellTicketC5==0 && 1170 + Zonepitch*6 > Bid  && Bid> 1170 + Zonepitch*5 ){
	SellTicketC5 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC5!=0 &&  Ask < 1170 + Zonepitch*4.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC5,SELECT_BY_TICKET);
	OrderClose(SellTicketC5,OrderLots(),Ask,3,Red);
	SellTicketC5 = 0;
}

if(SellTicketC6==0 && 1170 + Zonepitch*7 > Bid  && Bid> 1170 + Zonepitch*6 ){
	SellTicketC6 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC6!=0 &&  Ask < 1170 + Zonepitch*5.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC6,SELECT_BY_TICKET);
	OrderClose(SellTicketC6,OrderLots(),Ask,3,Red);
	SellTicketC6 = 0;
}

if(SellTicketC7==0 && 1170 + Zonepitch*8 > Bid  && Bid> 1170 + Zonepitch*7 ){
	SellTicketC7 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC7!=0 &&  Ask < 1170 + Zonepitch*6.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC7,SELECT_BY_TICKET);
	OrderClose(SellTicketC7,OrderLots(),Ask,3,Red);
	SellTicketC7 = 0;
}

if(SellTicketC8==0 && 1170 + Zonepitch*9 > Bid  && Bid> 1170 + Zonepitch*8 ){
	SellTicketC8 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC8!=0 &&  Ask < 1170 + Zonepitch*7.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC8,SELECT_BY_TICKET);
	OrderClose(SellTicketC8,OrderLots(),Ask,3,Red);
	SellTicketC8 = 0;
}

if(SellTicketC9==0 && 1170 + Zonepitch*10 > Bid  && Bid> 1170 + Zonepitch*9 ){
	SellTicketC9 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC9!=0 &&  Ask < 1170 + Zonepitch*8.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC9,SELECT_BY_TICKET);
	OrderClose(SellTicketC9,OrderLots(),Ask,3,Red);
	SellTicketC9 = 0;
}

if(SellTicketC10==0 && 1170 + Zonepitch*11 > Bid  && Bid> 1170 + Zonepitch*10 ){
	SellTicketC10 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC10!=0 &&  Ask < 1170 + Zonepitch*9.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC10,SELECT_BY_TICKET);
	OrderClose(SellTicketC10,OrderLots(),Ask,3,Red);
	SellTicketC10 = 0;
}

if(SellTicketC11==0 && 1170 + Zonepitch*12 > Bid  && Bid> 1170 + Zonepitch*11 ){
	SellTicketC11 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC11!=0 &&  Ask < 1170 + Zonepitch*10.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC11,SELECT_BY_TICKET);
	OrderClose(SellTicketC11,OrderLots(),Ask,3,Red);
	SellTicketC11 = 0;
}

if(SellTicketC12==0 && 1170 + Zonepitch*13 > Bid  && Bid> 1170 + Zonepitch*12 ){
	SellTicketC12 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC12!=0 &&  Ask < 1170 + Zonepitch*11.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC12,SELECT_BY_TICKET);
	OrderClose(SellTicketC12,OrderLots(),Ask,3,Red);
	SellTicketC12 = 0;
}

if(SellTicketC13==0 && 1170 + Zonepitch*14 > Bid  && Bid> 1170 + Zonepitch*13 ){
	SellTicketC13 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC13!=0 &&  Ask < 1170 + Zonepitch*12.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC13,SELECT_BY_TICKET);
	OrderClose(SellTicketC13,OrderLots(),Ask,3,Red);
	SellTicketC13 = 0;
}

if(SellTicketC14==0 && 1170 + Zonepitch*15 > Bid  && Bid> 1170 + Zonepitch*14 ){
	SellTicketC14 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC14!=0 &&  Ask < 1170 + Zonepitch*13.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC14,SELECT_BY_TICKET);
	OrderClose(SellTicketC14,OrderLots(),Ask,3,Red);
	SellTicketC14 = 0;
}

if(SellTicketC15==0 && 1170 + Zonepitch*16 > Bid  && Bid> 1170 + Zonepitch*15 ){
	SellTicketC15 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC15!=0 &&  Ask < 1170 + Zonepitch*14.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC15,SELECT_BY_TICKET);
	OrderClose(SellTicketC15,OrderLots(),Ask,3,Red);
	SellTicketC15 = 0;
}

if(SellTicketC16==0 && 1170 + Zonepitch*17 > Bid  && Bid> 1170 + Zonepitch*16 ){
	SellTicketC16 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC16!=0 &&  Ask < 1170 + Zonepitch*15.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC16,SELECT_BY_TICKET);
	OrderClose(SellTicketC16,OrderLots(),Ask,3,Red);
	SellTicketC16 = 0;
}

if(SellTicketC17==0 && 1170 + Zonepitch*18 > Bid  && Bid> 1170 + Zonepitch*17 ){
	SellTicketC17 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC17!=0 &&  Ask < 1170 + Zonepitch*16.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC17,SELECT_BY_TICKET);
	OrderClose(SellTicketC17,OrderLots(),Ask,3,Red);
	SellTicketC17 = 0;
}

if(SellTicketC18==0 && 1170 + Zonepitch*19 > Bid  && Bid> 1170 + Zonepitch*18 ){
	SellTicketC18 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC18!=0 &&  Ask < 1170 + Zonepitch*17.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC18,SELECT_BY_TICKET);
	OrderClose(SellTicketC18,OrderLots(),Ask,3,Red);
	SellTicketC18 = 0;
}

if(SellTicketC19==0 && 1170 + Zonepitch*20 > Bid  && Bid> 1170 + Zonepitch*19 ){
	SellTicketC19 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC19!=0 &&  Ask < 1170 + Zonepitch*18.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC19,SELECT_BY_TICKET);
	OrderClose(SellTicketC19,OrderLots(),Ask,3,Red);
	SellTicketC19 = 0;
}

if(SellTicketC20==0 && 1170 + Zonepitch*21 > Bid  && Bid> 1170 + Zonepitch*20 ){
	SellTicketC20 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC20!=0 &&  Ask < 1170 + Zonepitch*19.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC20,SELECT_BY_TICKET);
	OrderClose(SellTicketC20,OrderLots(),Ask,3,Red);
	SellTicketC20 = 0;
}

if(SellTicketC21==0 && 1170 + Zonepitch*22 > Bid  && Bid> 1170 + Zonepitch*21 ){
	SellTicketC21 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC21!=0 &&  Ask < 1170 + Zonepitch*20.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC21,SELECT_BY_TICKET);
	OrderClose(SellTicketC21,OrderLots(),Ask,3,Red);
	SellTicketC21 = 0;
}

if(SellTicketC22==0 && 1170 + Zonepitch*23 > Bid  && Bid> 1170 + Zonepitch*22 ){
	SellTicketC22 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC22!=0 &&  Ask < 1170 + Zonepitch*21.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC22,SELECT_BY_TICKET);
	OrderClose(SellTicketC22,OrderLots(),Ask,3,Red);
	SellTicketC22 = 0;
}

if(SellTicketC23==0 && 1170 + Zonepitch*24 > Bid  && Bid> 1170 + Zonepitch*23 ){
	SellTicketC23 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC23!=0 &&  Ask < 1170 + Zonepitch*22.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC23,SELECT_BY_TICKET);
	OrderClose(SellTicketC23,OrderLots(),Ask,3,Red);
	SellTicketC23 = 0;
}

if(SellTicketC24==0 && 1170 + Zonepitch*25 > Bid  && Bid> 1170 + Zonepitch*24 ){
	SellTicketC24 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC24!=0 &&  Ask < 1170 + Zonepitch*23.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC24,SELECT_BY_TICKET);
	OrderClose(SellTicketC24,OrderLots(),Ask,3,Red);
	SellTicketC24 = 0;
}

if(SellTicketC25==0 && 1170 + Zonepitch*26 > Bid  && Bid> 1170 + Zonepitch*25 ){
	SellTicketC25 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC25!=0 &&  Ask < 1170 + Zonepitch*24.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC25,SELECT_BY_TICKET);
	OrderClose(SellTicketC25,OrderLots(),Ask,3,Red);
	SellTicketC25 = 0;
}

if(SellTicketC26==0 && 1170 + Zonepitch*27 > Bid  && Bid> 1170 + Zonepitch*26 ){
	SellTicketC26 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC26!=0 &&  Ask < 1170 + Zonepitch*25.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC26,SELECT_BY_TICKET);
	OrderClose(SellTicketC26,OrderLots(),Ask,3,Red);
	SellTicketC26 = 0;
}

if(SellTicketC27==0 && 1170 + Zonepitch*28 > Bid  && Bid> 1170 + Zonepitch*27 ){
	SellTicketC27 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC27!=0 &&  Ask < 1170 + Zonepitch*26.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC27,SELECT_BY_TICKET);
	OrderClose(SellTicketC27,OrderLots(),Ask,3,Red);
	SellTicketC27 = 0;
}

if(SellTicketC28==0 && 1170 + Zonepitch*29 > Bid  && Bid> 1170 + Zonepitch*28 ){
	SellTicketC28 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC28!=0 &&  Ask < 1170 + Zonepitch*27.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC28,SELECT_BY_TICKET);
	OrderClose(SellTicketC28,OrderLots(),Ask,3,Red);
	SellTicketC28 = 0;
}

if(SellTicketC29==0 && 1170 + Zonepitch*30 > Bid  && Bid> 1170 + Zonepitch*29 ){
	SellTicketC29 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC29!=0 &&  Ask < 1170 + Zonepitch*28.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC29,SELECT_BY_TICKET);
	OrderClose(SellTicketC29,OrderLots(),Ask,3,Red);
	SellTicketC29 = 0;
}

if(SellTicketC30==0 && 1170 + Zonepitch*31 > Bid  && Bid> 1170 + Zonepitch*30 ){
	SellTicketC30 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC30!=0 &&  Ask < 1170 + Zonepitch*29.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC30,SELECT_BY_TICKET);
	OrderClose(SellTicketC30,OrderLots(),Ask,3,Red);
	SellTicketC30 = 0;
}

if(SellTicketC31==0 && 1170 + Zonepitch*32 > Bid  && Bid> 1170 + Zonepitch*31 ){
	SellTicketC31 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC31!=0 &&  Ask < 1170 + Zonepitch*30.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC31,SELECT_BY_TICKET);
	OrderClose(SellTicketC31,OrderLots(),Ask,3,Red);
	SellTicketC31 = 0;
}

if(SellTicketC32==0 && 1170 + Zonepitch*33 > Bid  && Bid> 1170 + Zonepitch*32 ){
	SellTicketC32 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC32!=0 &&  Ask < 1170 + Zonepitch*31.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC32,SELECT_BY_TICKET);
	OrderClose(SellTicketC32,OrderLots(),Ask,3,Red);
	SellTicketC32 = 0;
}

if(SellTicketC33==0 && 1170 + Zonepitch*34 > Bid  && Bid> 1170 + Zonepitch*33 ){
	SellTicketC33 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC33!=0 &&  Ask < 1170 + Zonepitch*32.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC33,SELECT_BY_TICKET);
	OrderClose(SellTicketC33,OrderLots(),Ask,3,Red);
	SellTicketC33 = 0;
}

if(SellTicketC34==0 && 1170 + Zonepitch*35 > Bid  && Bid> 1170 + Zonepitch*34 ){
	SellTicketC34 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC34!=0 &&  Ask < 1170 + Zonepitch*33.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC34,SELECT_BY_TICKET);
	OrderClose(SellTicketC34,OrderLots(),Ask,3,Red);
	SellTicketC34 = 0;
}

if(SellTicketC35==0 && 1170 + Zonepitch*36 > Bid  && Bid> 1170 + Zonepitch*35 ){
	SellTicketC35 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC35!=0 &&  Ask < 1170 + Zonepitch*34.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC35,SELECT_BY_TICKET);
	OrderClose(SellTicketC35,OrderLots(),Ask,3,Red);
	SellTicketC35 = 0;
}

if(SellTicketC36==0 && 1170 + Zonepitch*37 > Bid  && Bid> 1170 + Zonepitch*36 ){
	SellTicketC36 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC36!=0 &&  Ask < 1170 + Zonepitch*35.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC36,SELECT_BY_TICKET);
	OrderClose(SellTicketC36,OrderLots(),Ask,3,Red);
	SellTicketC36 = 0;
}

if(SellTicketC37==0 && 1170 + Zonepitch*38 > Bid  && Bid> 1170 + Zonepitch*37 ){
	SellTicketC37 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC37!=0 &&  Ask < 1170 + Zonepitch*36.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC37,SELECT_BY_TICKET);
	OrderClose(SellTicketC37,OrderLots(),Ask,3,Red);
	SellTicketC37 = 0;
}

if(SellTicketC38==0 && 1170 + Zonepitch*39 > Bid  && Bid> 1170 + Zonepitch*38 ){
	SellTicketC38 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC38!=0 &&  Ask < 1170 + Zonepitch*37.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC38,SELECT_BY_TICKET);
	OrderClose(SellTicketC38,OrderLots(),Ask,3,Red);
	SellTicketC38 = 0;
}

if(SellTicketC39==0 && 1170 + Zonepitch*40 > Bid  && Bid> 1170 + Zonepitch*39 ){
	SellTicketC39 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC39!=0 &&  Ask < 1170 + Zonepitch*38.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC39,SELECT_BY_TICKET);
	OrderClose(SellTicketC39,OrderLots(),Ask,3,Red);
	SellTicketC39 = 0;
}

if(SellTicketC40==0 && 1170 + Zonepitch*41 > Bid  && Bid> 1170 + Zonepitch*40 ){
	SellTicketC40 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC40!=0 &&  Ask < 1170 + Zonepitch*39.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC40,SELECT_BY_TICKET);
	OrderClose(SellTicketC40,OrderLots(),Ask,3,Red);
	SellTicketC40 = 0;
}

if(SellTicketC41==0 && 1170 + Zonepitch*42 > Bid  && Bid> 1170 + Zonepitch*41 ){
	SellTicketC41 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC41!=0 &&  Ask < 1170 + Zonepitch*40.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC41,SELECT_BY_TICKET);
	OrderClose(SellTicketC41,OrderLots(),Ask,3,Red);
	SellTicketC41 = 0;
}

if(SellTicketC42==0 && 1170 + Zonepitch*43 > Bid  && Bid> 1170 + Zonepitch*42 ){
	SellTicketC42 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC42!=0 &&  Ask < 1170 + Zonepitch*41.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC42,SELECT_BY_TICKET);
	OrderClose(SellTicketC42,OrderLots(),Ask,3,Red);
	SellTicketC42 = 0;
}

if(SellTicketC43==0 && 1170 + Zonepitch*44 > Bid  && Bid> 1170 + Zonepitch*43 ){
	SellTicketC43 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC43!=0 &&  Ask < 1170 + Zonepitch*42.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC43,SELECT_BY_TICKET);
	OrderClose(SellTicketC43,OrderLots(),Ask,3,Red);
	SellTicketC43 = 0;
}

if(SellTicketC44==0 && 1170 + Zonepitch*45 > Bid  && Bid> 1170 + Zonepitch*44 ){
	SellTicketC44 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC44!=0 &&  Ask < 1170 + Zonepitch*43.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC44,SELECT_BY_TICKET);
	OrderClose(SellTicketC44,OrderLots(),Ask,3,Red);
	SellTicketC44 = 0;
}

if(SellTicketC45==0 && 1170 + Zonepitch*46 > Bid  && Bid> 1170 + Zonepitch*45 ){
	SellTicketC45 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC45!=0 &&  Ask < 1170 + Zonepitch*44.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC45,SELECT_BY_TICKET);
	OrderClose(SellTicketC45,OrderLots(),Ask,3,Red);
	SellTicketC45 = 0;
}

if(SellTicketC46==0 && 1170 + Zonepitch*47 > Bid  && Bid> 1170 + Zonepitch*46 ){
	SellTicketC46 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC46!=0 &&  Ask < 1170 + Zonepitch*45.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC46,SELECT_BY_TICKET);
	OrderClose(SellTicketC46,OrderLots(),Ask,3,Red);
	SellTicketC46 = 0;
}

if(SellTicketC47==0 && 1170 + Zonepitch*48 > Bid  && Bid> 1170 + Zonepitch*47 ){
	SellTicketC47 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC47!=0 &&  Ask < 1170 + Zonepitch*46.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC47,SELECT_BY_TICKET);
	OrderClose(SellTicketC47,OrderLots(),Ask,3,Red);
	SellTicketC47 = 0;
}

if(SellTicketC48==0 && 1170 + Zonepitch*49 > Bid  && Bid> 1170 + Zonepitch*48 ){
	SellTicketC48 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC48!=0 &&  Ask < 1170 + Zonepitch*47.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC48,SELECT_BY_TICKET);
	OrderClose(SellTicketC48,OrderLots(),Ask,3,Red);
	SellTicketC48 = 0;
}

if(SellTicketC49==0 && 1170 + Zonepitch*50 > Bid  && Bid> 1170 + Zonepitch*49 ){
	SellTicketC49 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC49!=0 &&  Ask < 1170 + Zonepitch*48.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC49,SELECT_BY_TICKET);
	OrderClose(SellTicketC49,OrderLots(),Ask,3,Red);
	SellTicketC49 = 0;
}

if(SellTicketC50==0 && 1170 + Zonepitch*51 > Bid  && Bid> 1170 + Zonepitch*50 ){
	SellTicketC50 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC50!=0 &&  Ask < 1170 + Zonepitch*49.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC50,SELECT_BY_TICKET);
	OrderClose(SellTicketC50,OrderLots(),Ask,3,Red);
	SellTicketC50 = 0;
}

if(SellTicketC51==0 && 1170 + Zonepitch*52 > Bid  && Bid> 1170 + Zonepitch*51 ){
	SellTicketC51 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC51!=0 &&  Ask < 1170 + Zonepitch*50.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC51,SELECT_BY_TICKET);
	OrderClose(SellTicketC51,OrderLots(),Ask,3,Red);
	SellTicketC51 = 0;
}

if(SellTicketC52==0 && 1170 + Zonepitch*53 > Bid  && Bid> 1170 + Zonepitch*52 ){
	SellTicketC52 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC52!=0 &&  Ask < 1170 + Zonepitch*51.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC52,SELECT_BY_TICKET);
	OrderClose(SellTicketC52,OrderLots(),Ask,3,Red);
	SellTicketC52 = 0;
}

if(SellTicketC53==0 && 1170 + Zonepitch*54 > Bid  && Bid> 1170 + Zonepitch*53 ){
	SellTicketC53 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC53!=0 &&  Ask < 1170 + Zonepitch*52.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC53,SELECT_BY_TICKET);
	OrderClose(SellTicketC53,OrderLots(),Ask,3,Red);
	SellTicketC53 = 0;
}

if(SellTicketC54==0 && 1170 + Zonepitch*55 > Bid  && Bid> 1170 + Zonepitch*54 ){
	SellTicketC54 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC54!=0 &&  Ask < 1170 + Zonepitch*53.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC54,SELECT_BY_TICKET);
	OrderClose(SellTicketC54,OrderLots(),Ask,3,Red);
	SellTicketC54 = 0;
}

if(SellTicketC55==0 && 1170 + Zonepitch*56 > Bid  && Bid> 1170 + Zonepitch*55 ){
	SellTicketC55 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC55!=0 &&  Ask < 1170 + Zonepitch*54.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC55,SELECT_BY_TICKET);
	OrderClose(SellTicketC55,OrderLots(),Ask,3,Red);
	SellTicketC55 = 0;
}

if(SellTicketC56==0 && 1170 + Zonepitch*57 > Bid  && Bid> 1170 + Zonepitch*56 ){
	SellTicketC56 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC56!=0 &&  Ask < 1170 + Zonepitch*55.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC56,SELECT_BY_TICKET);
	OrderClose(SellTicketC56,OrderLots(),Ask,3,Red);
	SellTicketC56 = 0;
}

if(SellTicketC57==0 && 1170 + Zonepitch*58 > Bid  && Bid> 1170 + Zonepitch*57 ){
	SellTicketC57 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC57!=0 &&  Ask < 1170 + Zonepitch*56.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC57,SELECT_BY_TICKET);
	OrderClose(SellTicketC57,OrderLots(),Ask,3,Red);
	SellTicketC57 = 0;
}

if(SellTicketC58==0 && 1170 + Zonepitch*59 > Bid  && Bid> 1170 + Zonepitch*58 ){
	SellTicketC58 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC58!=0 &&  Ask < 1170 + Zonepitch*57.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC58,SELECT_BY_TICKET);
	OrderClose(SellTicketC58,OrderLots(),Ask,3,Red);
	SellTicketC58 = 0;
}

if(SellTicketC59==0 && 1170 + Zonepitch*60 > Bid  && Bid> 1170 + Zonepitch*59 ){
	SellTicketC59 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC59!=0 &&  Ask < 1170 + Zonepitch*58.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC59,SELECT_BY_TICKET);
	OrderClose(SellTicketC59,OrderLots(),Ask,3,Red);
	SellTicketC59 = 0;
}

if(SellTicketC60==0 && 1170 + Zonepitch*61 > Bid  && Bid> 1170 + Zonepitch*60 ){
	SellTicketC60 = OrderSend(Symbol(),OP_SELL,Lot,Bid, 3 ,0,0,0,MagicNumber,0,Red);
}
if(SellTicketC60!=0 &&  Ask < 1170 + Zonepitch*59.8  && Ask < EMA100  && NewBar && Ask <LOBB  ){
	OrderSelect(SellTicketC60,SELECT_BY_TICKET);
	OrderClose(SellTicketC60,OrderLots(),Ask,3,Red);
	SellTicketC60 = 0;
}

if(BuyTicketA1==0 && 1270 - Zonepitch*2 < Ask  && Ask< 1270 - Zonepitch*1 ){
	BuyTicketA1 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA1!=0 &&  Bid > 1270 - Zonepitch*0 ){
	OrderSelect(BuyTicketA1,SELECT_BY_TICKET);
	OrderClose(BuyTicketA1,OrderLots(),Bid,3,Green);
	BuyTicketA1 = 0;
}

if(BuyTicketA2==0 && 1270 - Zonepitch*3 < Ask  && Ask< 1270 - Zonepitch*2 ){
	BuyTicketA2 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA2!=0 &&  Bid > 1270 - Zonepitch*1 ){
	OrderSelect(BuyTicketA2,SELECT_BY_TICKET);
	OrderClose(BuyTicketA2,OrderLots(),Bid,3,Green);
	BuyTicketA2 = 0;
}

if(BuyTicketA3==0 && 1270 - Zonepitch*4 < Ask  && Ask< 1270 - Zonepitch*3 ){
	BuyTicketA3 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA3!=0 &&  Bid > 1270 - Zonepitch*2 ){
	OrderSelect(BuyTicketA3,SELECT_BY_TICKET);
	OrderClose(BuyTicketA3,OrderLots(),Bid,3,Green);
	BuyTicketA3 = 0;
}

if(BuyTicketA4==0 && 1270 - Zonepitch*5 < Ask  && Ask< 1270 - Zonepitch*4 ){
	BuyTicketA4 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA4!=0 &&  Bid > 1270 - Zonepitch*3 ){
	OrderSelect(BuyTicketA4,SELECT_BY_TICKET);
	OrderClose(BuyTicketA4,OrderLots(),Bid,3,Green);
	BuyTicketA4 = 0;
}


if(BuyTicketA5==0 && 1270 - Zonepitch*6 < Ask  && Ask< 1270 - Zonepitch*5 ){
	BuyTicketA5 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA5!=0 &&  Bid > 1270 - Zonepitch*4 ){
	OrderSelect(BuyTicketA5,SELECT_BY_TICKET);
	OrderClose(BuyTicketA5,OrderLots(),Bid,3,Green);
	BuyTicketA5 = 0;
}

if(BuyTicketA6==0 && 1270 - Zonepitch*7 < Ask  && Ask< 1270 - Zonepitch*6 ){
	BuyTicketA6 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA6!=0 &&  Bid > 1270 - Zonepitch*5 ){
	OrderSelect(BuyTicketA6,SELECT_BY_TICKET);
	OrderClose(BuyTicketA6,OrderLots(),Bid,3,Green);
	BuyTicketA6 = 0;
}

if(BuyTicketA7==0 && 1270 - Zonepitch*8 < Ask  && Ask< 1270 - Zonepitch*7 ){
	BuyTicketA7 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA7!=0 &&  Bid > 1270 - Zonepitch*6 ){
	OrderSelect(BuyTicketA7,SELECT_BY_TICKET);
	OrderClose(BuyTicketA7,OrderLots(),Bid,3,Green);
	BuyTicketA7 = 0;
}

if(BuyTicketA8==0 && 1270 - Zonepitch*9 < Ask  && Ask< 1270 - Zonepitch*8 ){
	BuyTicketA8 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA8!=0 &&  Bid > 1270 - Zonepitch*7 ){
	OrderSelect(BuyTicketA8,SELECT_BY_TICKET);
	OrderClose(BuyTicketA8,OrderLots(),Bid,3,Green);
	BuyTicketA8 = 0;
}

if(BuyTicketA9==0 && 1270 - Zonepitch*10 < Ask  && Ask< 1270 - Zonepitch*9 ){
	BuyTicketA9 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA9!=0 &&  Bid > 1270 - Zonepitch*8 ){
	OrderSelect(BuyTicketA9,SELECT_BY_TICKET);
	OrderClose(BuyTicketA9,OrderLots(),Bid,3,Green);
	BuyTicketA9 = 0;
}

if(BuyTicketA10==0 && 1270 - Zonepitch*11 < Ask  && Ask< 1270 - Zonepitch*10 ){
	BuyTicketA10 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA10!=0 &&  Bid > 1270 - Zonepitch*9 ){
	OrderSelect(BuyTicketA10,SELECT_BY_TICKET);
	OrderClose(BuyTicketA10,OrderLots(),Bid,3,Green);
	BuyTicketA10 = 0;
}

if(BuyTicketA11==0 && 1270 - Zonepitch*12 < Ask  && Ask< 1270 - Zonepitch*11 ){
	BuyTicketA11 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA11!=0 &&  Bid > 1270 - Zonepitch*10 ){
	OrderSelect(BuyTicketA11,SELECT_BY_TICKET);
	OrderClose(BuyTicketA11,OrderLots(),Bid,3,Green);
	BuyTicketA11 = 0;
}

if(BuyTicketA12==0 && 1270 - Zonepitch*13 < Ask  && Ask< 1270 - Zonepitch*12 ){
	BuyTicketA12 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA12!=0 &&  Bid > 1270 - Zonepitch*11 ){
	OrderSelect(BuyTicketA12,SELECT_BY_TICKET);
	OrderClose(BuyTicketA12,OrderLots(),Bid,3,Green);
	BuyTicketA12 = 0;
}

if(BuyTicketA13==0 && 1270 - Zonepitch*14 < Ask  && Ask< 1270 - Zonepitch*13 ){
	BuyTicketA13 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA13!=0 &&  Bid > 1270 - Zonepitch*12 ){
	OrderSelect(BuyTicketA13,SELECT_BY_TICKET);
	OrderClose(BuyTicketA13,OrderLots(),Bid,3,Green);
	BuyTicketA13 = 0;
}

if(BuyTicketA14==0 && 1270 - Zonepitch*15 < Ask  && Ask< 1270 - Zonepitch*14 ){
	BuyTicketA14 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA14!=0 &&  Bid > 1270 - Zonepitch*13 ){
	OrderSelect(BuyTicketA14,SELECT_BY_TICKET);
	OrderClose(BuyTicketA14,OrderLots(),Bid,3,Green);
	BuyTicketA14 = 0;
}

if(BuyTicketA15==0 && 1270 - Zonepitch*16 < Ask  && Ask< 1270 - Zonepitch*15 ){
	BuyTicketA15 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA15!=0 &&  Bid > 1270 - Zonepitch*14 ){
	OrderSelect(BuyTicketA15,SELECT_BY_TICKET);
	OrderClose(BuyTicketA15,OrderLots(),Bid,3,Green);
	BuyTicketA15 = 0;
}

if(BuyTicketA16==0 && 1270 - Zonepitch*17 < Ask  && Ask< 1270 - Zonepitch*16 ){
	BuyTicketA16 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA16!=0 &&  Bid > 1270 - Zonepitch*15 ){
	OrderSelect(BuyTicketA16,SELECT_BY_TICKET);
	OrderClose(BuyTicketA16,OrderLots(),Bid,3,Green);
	BuyTicketA16 = 0;
}

if(BuyTicketA17==0 && 1270 - Zonepitch*18 < Ask  && Ask< 1270 - Zonepitch*17 ){
	BuyTicketA17 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA17!=0 &&  Bid > 1270 - Zonepitch*16 ){
	OrderSelect(BuyTicketA17,SELECT_BY_TICKET);
	OrderClose(BuyTicketA17,OrderLots(),Bid,3,Green);
	BuyTicketA17 = 0;
}

if(BuyTicketA18==0 && 1270 - Zonepitch*19 < Ask  && Ask< 1270 - Zonepitch*18 ){
	BuyTicketA18 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA18!=0 &&  Bid > 1270 - Zonepitch*17 ){
	OrderSelect(BuyTicketA18,SELECT_BY_TICKET);
	OrderClose(BuyTicketA18,OrderLots(),Bid,3,Green);
	BuyTicketA18 = 0;
}

if(BuyTicketA19==0 && 1270 - Zonepitch*20 < Ask  && Ask< 1270 - Zonepitch*19 ){
	BuyTicketA19 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA19!=0 &&  Bid > 1270 - Zonepitch*18 ){
	OrderSelect(BuyTicketA19,SELECT_BY_TICKET);
	OrderClose(BuyTicketA19,OrderLots(),Bid,3,Green);
	BuyTicketA19 = 0;
}

if(BuyTicketA20==0 && 1270 - Zonepitch*21 < Ask  && Ask< 1270 - Zonepitch*20 ){
	BuyTicketA20 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA20!=0 &&  Bid > 1270 - Zonepitch*19 ){
	OrderSelect(BuyTicketA20,SELECT_BY_TICKET);
	OrderClose(BuyTicketA20,OrderLots(),Bid,3,Green);
	BuyTicketA20 = 0;
}

if(BuyTicketA21==0 && 1270 - Zonepitch*22 < Ask  && Ask< 1270 - Zonepitch*21 ){
	BuyTicketA21 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA21!=0 &&  Bid > 1270 - Zonepitch*20 ){
	OrderSelect(BuyTicketA21,SELECT_BY_TICKET);
	OrderClose(BuyTicketA21,OrderLots(),Bid,3,Green);
	BuyTicketA21 = 0;
}

if(BuyTicketA22==0 && 1270 - Zonepitch*23 < Ask  && Ask< 1270 - Zonepitch*22 ){
	BuyTicketA22 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA22!=0 &&  Bid > 1270 - Zonepitch*21 ){
	OrderSelect(BuyTicketA22,SELECT_BY_TICKET);
	OrderClose(BuyTicketA22,OrderLots(),Bid,3,Green);
	BuyTicketA22 = 0;
}

if(BuyTicketA23==0 && 1270 - Zonepitch*24 < Ask  && Ask< 1270 - Zonepitch*23 ){
	BuyTicketA23 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA23!=0 &&  Bid > 1270 - Zonepitch*22 ){
	OrderSelect(BuyTicketA23,SELECT_BY_TICKET);
	OrderClose(BuyTicketA23,OrderLots(),Bid,3,Green);
	BuyTicketA23 = 0;
}

if(BuyTicketA24==0 && 1270 - Zonepitch*25 < Ask  && Ask< 1270 - Zonepitch*24 ){
	BuyTicketA24 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA24!=0 &&  Bid > 1270 - Zonepitch*23 ){
	OrderSelect(BuyTicketA24,SELECT_BY_TICKET);
	OrderClose(BuyTicketA24,OrderLots(),Bid,3,Green);
	BuyTicketA24 = 0;
}

if(BuyTicketA25==0 && 1270 - Zonepitch*26 < Ask  && Ask< 1270 - Zonepitch*25 ){
	BuyTicketA25 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA25!=0 &&  Bid > 1270 - Zonepitch*24 ){
	OrderSelect(BuyTicketA25,SELECT_BY_TICKET);
	OrderClose(BuyTicketA25,OrderLots(),Bid,3,Green);
	BuyTicketA25 = 0;
}

if(BuyTicketA26==0 && 1270 - Zonepitch*27 < Ask  && Ask< 1270 - Zonepitch*26 ){
	BuyTicketA26 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA26!=0 &&  Bid > 1270 - Zonepitch*25 ){
	OrderSelect(BuyTicketA26,SELECT_BY_TICKET);
	OrderClose(BuyTicketA26,OrderLots(),Bid,3,Green);
	BuyTicketA26 = 0;
}

if(BuyTicketA27==0 && 1270 - Zonepitch*28 < Ask  && Ask< 1270 - Zonepitch*27 ){
	BuyTicketA27 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA27!=0 &&  Bid > 1270 - Zonepitch*26 ){
	OrderSelect(BuyTicketA27,SELECT_BY_TICKET);
	OrderClose(BuyTicketA27,OrderLots(),Bid,3,Green);
	BuyTicketA27 = 0;
}

if(BuyTicketA28==0 && 1270 - Zonepitch*29 < Ask  && Ask< 1270 - Zonepitch*28 ){
	BuyTicketA28 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA28!=0 &&  Bid > 1270 - Zonepitch*27 ){
	OrderSelect(BuyTicketA28,SELECT_BY_TICKET);
	OrderClose(BuyTicketA28,OrderLots(),Bid,3,Green);
	BuyTicketA28 = 0;
}

if(BuyTicketA29==0 && 1270 - Zonepitch*30 < Ask  && Ask< 1270 - Zonepitch*29 ){
	BuyTicketA29 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA29!=0 &&  Bid > 1270 - Zonepitch*28 ){
	OrderSelect(BuyTicketA29,SELECT_BY_TICKET);
	OrderClose(BuyTicketA29,OrderLots(),Bid,3,Green);
	BuyTicketA29 = 0;
}

if(BuyTicketA30==0 && 1270 - Zonepitch*31 < Ask  && Ask< 1270 - Zonepitch*30 ){
	BuyTicketA30 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA30!=0 &&  Bid > 1270 - Zonepitch*29 ){
	OrderSelect(BuyTicketA30,SELECT_BY_TICKET);
	OrderClose(BuyTicketA30,OrderLots(),Bid,3,Green);
	BuyTicketA30 = 0;
}

if(BuyTicketA31==0 && 1270 - Zonepitch*32 < Ask  && Ask< 1270 - Zonepitch*31 ){
	BuyTicketA31 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA31!=0 &&  Bid > 1270 - Zonepitch*30 ){
	OrderSelect(BuyTicketA31,SELECT_BY_TICKET);
	OrderClose(BuyTicketA31,OrderLots(),Bid,3,Green);
	BuyTicketA31 = 0;
}

if(BuyTicketA32==0 && 1270 - Zonepitch*33 < Ask  && Ask< 1270 - Zonepitch*32 ){
	BuyTicketA32 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA32!=0 &&  Bid > 1270 - Zonepitch*31 ){
	OrderSelect(BuyTicketA32,SELECT_BY_TICKET);
	OrderClose(BuyTicketA32,OrderLots(),Bid,3,Green);
	BuyTicketA32 = 0;
}

if(BuyTicketA33==0 && 1270 - Zonepitch*34 < Ask  && Ask< 1270 - Zonepitch*33 ){
	BuyTicketA33 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA33!=0 &&  Bid > 1270 - Zonepitch*32 ){
	OrderSelect(BuyTicketA33,SELECT_BY_TICKET);
	OrderClose(BuyTicketA33,OrderLots(),Bid,3,Green);
	BuyTicketA33 = 0;
}

if(BuyTicketA34==0 && 1270 - Zonepitch*35 < Ask  && Ask< 1270 - Zonepitch*34 ){
	BuyTicketA34 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA34!=0 &&  Bid > 1270 - Zonepitch*33 ){
	OrderSelect(BuyTicketA34,SELECT_BY_TICKET);
	OrderClose(BuyTicketA34,OrderLots(),Bid,3,Green);
	BuyTicketA34 = 0;
}

if(BuyTicketA35==0 && 1270 - Zonepitch*36 < Ask  && Ask< 1270 - Zonepitch*35 ){
	BuyTicketA35 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA35!=0 &&  Bid > 1270 - Zonepitch*34 ){
	OrderSelect(BuyTicketA35,SELECT_BY_TICKET);
	OrderClose(BuyTicketA35,OrderLots(),Bid,3,Green);
	BuyTicketA35 = 0;
}

if(BuyTicketA36==0 && 1270 - Zonepitch*37 < Ask  && Ask< 1270 - Zonepitch*36 ){
	BuyTicketA36 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA36!=0 &&  Bid > 1270 - Zonepitch*35 ){
	OrderSelect(BuyTicketA36,SELECT_BY_TICKET);
	OrderClose(BuyTicketA36,OrderLots(),Bid,3,Green);
	BuyTicketA36 = 0;
}


if(BuyTicketA37==0 && 1270 - Zonepitch*38 < Ask  && Ask< 1270 - Zonepitch*37 ){
	BuyTicketA37 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA37!=0 &&  Bid > 1270 - Zonepitch*36 ){
	OrderSelect(BuyTicketA37,SELECT_BY_TICKET);
	OrderClose(BuyTicketA37,OrderLots(),Bid,3,Green);
	BuyTicketA37 = 0;
}

if(BuyTicketA38==0 && 1270 - Zonepitch*39 < Ask  && Ask< 1270 - Zonepitch*38 ){
	BuyTicketA38 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA38!=0 &&  Bid > 1270 - Zonepitch*37 ){
	OrderSelect(BuyTicketA38,SELECT_BY_TICKET);
	OrderClose(BuyTicketA38,OrderLots(),Bid,3,Green);
	BuyTicketA38 = 0;
}

if(BuyTicketA39==0 && 1270 - Zonepitch*40 < Ask  && Ask< 1270 - Zonepitch*39 ){
	BuyTicketA39 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA39!=0 &&  Bid > 1270 - Zonepitch*38 ){
	OrderSelect(BuyTicketA39,SELECT_BY_TICKET);
	OrderClose(BuyTicketA39,OrderLots(),Bid,3,Green);
	BuyTicketA39 = 0;
}

if(BuyTicketA40==0 && 1270 - Zonepitch*41 < Ask  && Ask< 1270 - Zonepitch*40 ){
	BuyTicketA40 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA40!=0 &&  Bid > 1270 - Zonepitch*39 ){
	OrderSelect(BuyTicketA40,SELECT_BY_TICKET);
	OrderClose(BuyTicketA40,OrderLots(),Bid,3,Green);
	BuyTicketA40 = 0;
}

if(BuyTicketA41==0 && 1270 - Zonepitch*42 < Ask  && Ask< 1270 - Zonepitch*41 ){
	BuyTicketA41 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA41!=0 &&  Bid > 1270 - Zonepitch*40 ){
	OrderSelect(BuyTicketA41,SELECT_BY_TICKET);
	OrderClose(BuyTicketA41,OrderLots(),Bid,3,Green);
	BuyTicketA41 = 0;
}

if(BuyTicketA42==0 && 1270 - Zonepitch*43 < Ask  && Ask< 1270 - Zonepitch*42 ){
	BuyTicketA42 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA42!=0 &&  Bid > 1270 - Zonepitch*41 ){
	OrderSelect(BuyTicketA42,SELECT_BY_TICKET);
	OrderClose(BuyTicketA42,OrderLots(),Bid,3,Green);
	BuyTicketA42 = 0;
}

if(BuyTicketA43==0 && 1270 - Zonepitch*44 < Ask  && Ask< 1270 - Zonepitch*43 ){
	BuyTicketA43 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA43!=0 &&  Bid > 1270 - Zonepitch*42 ){
	OrderSelect(BuyTicketA43,SELECT_BY_TICKET);
	OrderClose(BuyTicketA43,OrderLots(),Bid,3,Green);
	BuyTicketA43 = 0;
}

if(BuyTicketA44==0 && 1270 - Zonepitch*45 < Ask  && Ask< 1270 - Zonepitch*44 ){
	BuyTicketA44 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA44!=0 &&  Bid > 1270 - Zonepitch*43 ){
	OrderSelect(BuyTicketA44,SELECT_BY_TICKET);
	OrderClose(BuyTicketA44,OrderLots(),Bid,3,Green);
	BuyTicketA44 = 0;
}

if(BuyTicketA45==0 && 1270 - Zonepitch*46 < Ask  && Ask< 1270 - Zonepitch*45 ){
	BuyTicketA45 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA45!=0 &&  Bid > 1270 - Zonepitch*44 ){
	OrderSelect(BuyTicketA45,SELECT_BY_TICKET);
	OrderClose(BuyTicketA45,OrderLots(),Bid,3,Green);
	BuyTicketA45 = 0;
}

if(BuyTicketA46==0 && 1270 - Zonepitch*47 < Ask  && Ask< 1270 - Zonepitch*46 ){
	BuyTicketA46 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA46!=0 &&  Bid > 1270 - Zonepitch*45 ){
	OrderSelect(BuyTicketA46,SELECT_BY_TICKET);
	OrderClose(BuyTicketA46,OrderLots(),Bid,3,Green);
	BuyTicketA46 = 0;
}

if(BuyTicketA47==0 && 1270 - Zonepitch*48 < Ask  && Ask< 1270 - Zonepitch*47 ){
	BuyTicketA47 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA47!=0 &&  Bid > 1270 - Zonepitch*46 ){
	OrderSelect(BuyTicketA47,SELECT_BY_TICKET);
	OrderClose(BuyTicketA47,OrderLots(),Bid,3,Green);
	BuyTicketA47 = 0;
}

if(BuyTicketA48==0 && 1270 - Zonepitch*49 < Ask  && Ask< 1270 - Zonepitch*48 ){
	BuyTicketA48 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA48!=0 &&  Bid > 1270 - Zonepitch*47 ){
	OrderSelect(BuyTicketA48,SELECT_BY_TICKET);
	OrderClose(BuyTicketA48,OrderLots(),Bid,3,Green);
	BuyTicketA48 = 0;
}

if(BuyTicketA49==0 && 1270 - Zonepitch*50 < Ask  && Ask< 1270 - Zonepitch*49 ){
	BuyTicketA49 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA49!=0 &&  Bid > 1270 - Zonepitch*48 ){
	OrderSelect(BuyTicketA49,SELECT_BY_TICKET);
	OrderClose(BuyTicketA49,OrderLots(),Bid,3,Green);
	BuyTicketA49 = 0;
}

if(BuyTicketA50==0 && 1270 - Zonepitch*51 < Ask  && Ask< 1270 - Zonepitch*50 ){
	BuyTicketA50 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA50!=0 &&  Bid > 1270 - Zonepitch*49 ){
	OrderSelect(BuyTicketA50,SELECT_BY_TICKET);
	OrderClose(BuyTicketA50,OrderLots(),Bid,3,Green);
	BuyTicketA50 = 0;
}

if(BuyTicketA51==0 && 1270 - Zonepitch*52 < Ask  && Ask< 1270 - Zonepitch*51 ){
	BuyTicketA51 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA51!=0 &&  Bid > 1270 - Zonepitch*50 ){
	OrderSelect(BuyTicketA51,SELECT_BY_TICKET);
	OrderClose(BuyTicketA51,OrderLots(),Bid,3,Green);
	BuyTicketA51 = 0;
}

if(BuyTicketA52==0 && 1270 - Zonepitch*53 < Ask  && Ask< 1270 - Zonepitch*52 ){
	BuyTicketA52 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA52!=0 &&  Bid > 1270 - Zonepitch*51 ){
	OrderSelect(BuyTicketA52,SELECT_BY_TICKET);
	OrderClose(BuyTicketA52,OrderLots(),Bid,3,Green);
	BuyTicketA52 = 0;
}

if(BuyTicketA53==0 && 1270 - Zonepitch*54 < Ask  && Ask< 1270 - Zonepitch*53 ){
	BuyTicketA53 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA53!=0 &&  Bid > 1270 - Zonepitch*52 ){
	OrderSelect(BuyTicketA53,SELECT_BY_TICKET);
	OrderClose(BuyTicketA53,OrderLots(),Bid,3,Green);
	BuyTicketA53 = 0;
}

if(BuyTicketA54==0 && 1270 - Zonepitch*55 < Ask  && Ask< 1270 - Zonepitch*54 ){
	BuyTicketA54 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA54!=0 &&  Bid > 1270 - Zonepitch*53 ){
	OrderSelect(BuyTicketA54,SELECT_BY_TICKET);
	OrderClose(BuyTicketA54,OrderLots(),Bid,3,Green);
	BuyTicketA54 = 0;
}

if(BuyTicketA55==0 && 1270 - Zonepitch*56 < Ask  && Ask< 1270 - Zonepitch*55 ){
	BuyTicketA55 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA55!=0 &&  Bid > 1270 - Zonepitch*54 ){
	OrderSelect(BuyTicketA55,SELECT_BY_TICKET);
	OrderClose(BuyTicketA55,OrderLots(),Bid,3,Green);
	BuyTicketA55 = 0;
}

if(BuyTicketA56==0 && 1270 - Zonepitch*57 < Ask  && Ask< 1270 - Zonepitch*56 ){
	BuyTicketA56 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA56!=0 &&  Bid > 1270 - Zonepitch*55 ){
	OrderSelect(BuyTicketA56,SELECT_BY_TICKET);
	OrderClose(BuyTicketA56,OrderLots(),Bid,3,Green);
	BuyTicketA56 = 0;
}

if(BuyTicketA57==0 && 1270 - Zonepitch*58 < Ask  && Ask< 1270 - Zonepitch*57 ){
	BuyTicketA57 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA57!=0 &&  Bid > 1270 - Zonepitch*56 ){
	OrderSelect(BuyTicketA57,SELECT_BY_TICKET);
	OrderClose(BuyTicketA57,OrderLots(),Bid,3,Green);
	BuyTicketA57 = 0;
}

if(BuyTicketA58==0 && 1270 - Zonepitch*59 < Ask  && Ask< 1270 - Zonepitch*58 ){
	BuyTicketA58 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA58!=0 &&  Bid > 1270 - Zonepitch*57 ){
	OrderSelect(BuyTicketA58,SELECT_BY_TICKET);
	OrderClose(BuyTicketA58,OrderLots(),Bid,3,Green);
	BuyTicketA58 = 0;
}

if(BuyTicketA59==0 && 1270 - Zonepitch*60 < Ask  && Ask< 1270 - Zonepitch*59 ){
	BuyTicketA59 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA59!=0 &&  Bid > 1270 - Zonepitch*58 ){
	OrderSelect(BuyTicketA59,SELECT_BY_TICKET);
	OrderClose(BuyTicketA59,OrderLots(),Bid,3,Green);
	BuyTicketA59 = 0;
}

if(BuyTicketA60==0 && 1270 - Zonepitch*61 < Ask  && Ask< 1270 - Zonepitch*60 ){
	BuyTicketA60 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketA60!=0 &&  Bid > 1270 - Zonepitch*59 ){
	OrderSelect(BuyTicketA60,SELECT_BY_TICKET);
	OrderClose(BuyTicketA60,OrderLots(),Bid,3,Green);
	BuyTicketA60 = 0;
}

if(BuyTicketB1==0 && 1270 - Zonepitch*2 < Ask  && Ask< 1270 - Zonepitch*1 ){
	BuyTicketB1 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB1!=0 &&  Bid > 1270 - Zonepitch*0.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB1,SELECT_BY_TICKET);
	OrderClose(BuyTicketB1,OrderLots(),Bid,3,Green);
	BuyTicketB1 = 0;
}

if(BuyTicketB2==0 && 1270 - Zonepitch*3 < Ask  && Ask< 1270 - Zonepitch*2 ){
	BuyTicketB2 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB2!=0 &&  Bid > 1270 - Zonepitch*1.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB2,SELECT_BY_TICKET);
	OrderClose(BuyTicketB2,OrderLots(),Bid,3,Green);
	BuyTicketB2 = 0;
}

if(BuyTicketB3==0 && 1270 - Zonepitch*4 < Ask  && Ask< 1270 - Zonepitch*3 ){
	BuyTicketB3 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB3!=0 &&  Bid > 1270 - Zonepitch*2.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB3,SELECT_BY_TICKET);
	OrderClose(BuyTicketB3,OrderLots(),Bid,3,Green);
	BuyTicketB3 = 0;
}

if(BuyTicketB4==0 && 1270 - Zonepitch*5 < Ask  && Ask< 1270 - Zonepitch*4 ){
	BuyTicketB4 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB4!=0 &&  Bid > 1270 - Zonepitch*3.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB4,SELECT_BY_TICKET);
	OrderClose(BuyTicketB4,OrderLots(),Bid,3,Green);
	BuyTicketB4 = 0;
}

if(BuyTicketB5==0 && 1270 - Zonepitch*6 < Ask  && Ask< 1270 - Zonepitch*5 ){
	BuyTicketB5 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB5!=0 &&  Bid > 1270 - Zonepitch*4.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB5,SELECT_BY_TICKET);
	OrderClose(BuyTicketB5,OrderLots(),Bid,3,Green);
	BuyTicketB5 = 0;
}

if(BuyTicketB6==0 && 1270 - Zonepitch*7 < Ask  && Ask< 1270 - Zonepitch*6 ){
	BuyTicketB6 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB6!=0 &&  Bid > 1270 - Zonepitch*5.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB6,SELECT_BY_TICKET);
	OrderClose(BuyTicketB6,OrderLots(),Bid,3,Green);
	BuyTicketB6 = 0;
}

if(BuyTicketB7==0 && 1270 - Zonepitch*8 < Ask  && Ask< 1270 - Zonepitch*7 ){
	BuyTicketB7 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB7!=0 &&  Bid > 1270 - Zonepitch*6.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB7,SELECT_BY_TICKET);
	OrderClose(BuyTicketB7,OrderLots(),Bid,3,Green);
	BuyTicketB7 = 0;
}

if(BuyTicketB8==0 && 1270 - Zonepitch*9 < Ask  && Ask< 1270 - Zonepitch*8 ){
	BuyTicketB8 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB8!=0 &&  Bid > 1270 - Zonepitch*7.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB8,SELECT_BY_TICKET);
	OrderClose(BuyTicketB8,OrderLots(),Bid,3,Green);
	BuyTicketB8 = 0;
}

if(BuyTicketB9==0 && 1270 - Zonepitch*10 < Ask  && Ask< 1270 - Zonepitch*9 ){
	BuyTicketB9 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB9!=0 &&  Bid > 1270 - Zonepitch*8.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB9,SELECT_BY_TICKET);
	OrderClose(BuyTicketB9,OrderLots(),Bid,3,Green);
	BuyTicketB9 = 0;
}

if(BuyTicketB10==0 && 1270 - Zonepitch*11 < Ask  && Ask< 1270 - Zonepitch*10 ){
	BuyTicketB10 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB10!=0 &&  Bid > 1270 - Zonepitch*9.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB10,SELECT_BY_TICKET);
	OrderClose(BuyTicketB10,OrderLots(),Bid,3,Green);
	BuyTicketB10 = 0;
}

if(BuyTicketB11==0 && 1270 - Zonepitch*12 < Ask  && Ask< 1270 - Zonepitch*11 ){
	BuyTicketB11 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB11!=0 &&  Bid > 1270 - Zonepitch*10.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB11,SELECT_BY_TICKET);
	OrderClose(BuyTicketB11,OrderLots(),Bid,3,Green);
	BuyTicketB11 = 0;
}

if(BuyTicketB12==0 && 1270 - Zonepitch*13 < Ask  && Ask< 1270 - Zonepitch*12 ){
	BuyTicketB12 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB12!=0 &&  Bid > 1270 - Zonepitch*11.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB12,SELECT_BY_TICKET);
	OrderClose(BuyTicketB12,OrderLots(),Bid,3,Green);
	BuyTicketB12 = 0;
}

if(BuyTicketB13==0 && 1270 - Zonepitch*14 < Ask  && Ask< 1270 - Zonepitch*13 ){
	BuyTicketB13 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB13!=0 &&  Bid > 1270 - Zonepitch*12.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB13,SELECT_BY_TICKET);
	OrderClose(BuyTicketB13,OrderLots(),Bid,3,Green);
	BuyTicketB13 = 0;
}

if(BuyTicketB14==0 && 1270 - Zonepitch*15 < Ask  && Ask< 1270 - Zonepitch*14 ){
	BuyTicketB14 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB14!=0 &&  Bid > 1270 - Zonepitch*13.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB14,SELECT_BY_TICKET);
	OrderClose(BuyTicketB14,OrderLots(),Bid,3,Green);
	BuyTicketB14 = 0;
}

if(BuyTicketB15==0 && 1270 - Zonepitch*16 < Ask  && Ask< 1270 - Zonepitch*15 ){
	BuyTicketB15 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB15!=0 &&  Bid > 1270 - Zonepitch*14.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB15,SELECT_BY_TICKET);
	OrderClose(BuyTicketB15,OrderLots(),Bid,3,Green);
	BuyTicketB15 = 0;
}

if(BuyTicketB16==0 && 1270 - Zonepitch*17 < Ask  && Ask< 1270 - Zonepitch*16 ){
	BuyTicketB16 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB16!=0 &&  Bid > 1270 - Zonepitch*15.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB16,SELECT_BY_TICKET);
	OrderClose(BuyTicketB16,OrderLots(),Bid,3,Green);
	BuyTicketB16 = 0;
}

if(BuyTicketB17==0 && 1270 - Zonepitch*18 < Ask  && Ask< 1270 - Zonepitch*17 ){
	BuyTicketB17 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB17!=0 &&  Bid > 1270 - Zonepitch*16.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB17,SELECT_BY_TICKET);
	OrderClose(BuyTicketB17,OrderLots(),Bid,3,Green);
	BuyTicketB17 = 0;
}

if(BuyTicketB18==0 && 1270 - Zonepitch*19 < Ask  && Ask< 1270 - Zonepitch*18 ){
	BuyTicketB18 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB18!=0 &&  Bid > 1270 - Zonepitch*17.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB18,SELECT_BY_TICKET);
	OrderClose(BuyTicketB18,OrderLots(),Bid,3,Green);
	BuyTicketB18 = 0;
}

if(BuyTicketB19==0 && 1270 - Zonepitch*20 < Ask  && Ask< 1270 - Zonepitch*19 ){
	BuyTicketB19 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB19!=0 &&  Bid > 1270 - Zonepitch*18.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB19,SELECT_BY_TICKET);
	OrderClose(BuyTicketB19,OrderLots(),Bid,3,Green);
	BuyTicketB19 = 0;
}

if(BuyTicketB20==0 && 1270 - Zonepitch*21 < Ask  && Ask< 1270 - Zonepitch*20 ){
	BuyTicketB20 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB20!=0 &&  Bid > 1270 - Zonepitch*19.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB20,SELECT_BY_TICKET);
	OrderClose(BuyTicketB20,OrderLots(),Bid,3,Green);
	BuyTicketB20 = 0;
}

if(BuyTicketB21==0 && 1270 - Zonepitch*22 < Ask  && Ask< 1270 - Zonepitch*21 ){
	BuyTicketB21 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB21!=0 &&  Bid > 1270 - Zonepitch*20.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB21,SELECT_BY_TICKET);
	OrderClose(BuyTicketB21,OrderLots(),Bid,3,Green);
	BuyTicketB21 = 0;
}

if(BuyTicketB22==0 && 1270 - Zonepitch*23 < Ask  && Ask< 1270 - Zonepitch*22 ){
	BuyTicketB22 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB22!=0 &&  Bid > 1270 - Zonepitch*21.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB22,SELECT_BY_TICKET);
	OrderClose(BuyTicketB22,OrderLots(),Bid,3,Green);
	BuyTicketB22 = 0;
}

if(BuyTicketB23==0 && 1270 - Zonepitch*24 < Ask  && Ask< 1270 - Zonepitch*23 ){
	BuyTicketB23 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB23!=0 &&  Bid > 1270 - Zonepitch*22.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB23,SELECT_BY_TICKET);
	OrderClose(BuyTicketB23,OrderLots(),Bid,3,Green);
	BuyTicketB23 = 0;
}

if(BuyTicketB24==0 && 1270 - Zonepitch*25 < Ask  && Ask< 1270 - Zonepitch*24 ){
	BuyTicketB24 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB24!=0 &&  Bid > 1270 - Zonepitch*23.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB24,SELECT_BY_TICKET);
	OrderClose(BuyTicketB24,OrderLots(),Bid,3,Green);
	BuyTicketB24 = 0;
}

if(BuyTicketB25==0 && 1270 - Zonepitch*26 < Ask  && Ask< 1270 - Zonepitch*25 ){
	BuyTicketB25 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB25!=0 &&  Bid > 1270 - Zonepitch*24.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB25,SELECT_BY_TICKET);
	OrderClose(BuyTicketB25,OrderLots(),Bid,3,Green);
	BuyTicketB25 = 0;
}

if(BuyTicketB26==0 && 1270 - Zonepitch*27 < Ask  && Ask< 1270 - Zonepitch*26 ){
	BuyTicketB26 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB26!=0 &&  Bid > 1270 - Zonepitch*25.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB26,SELECT_BY_TICKET);
	OrderClose(BuyTicketB26,OrderLots(),Bid,3,Green);
	BuyTicketB26 = 0;
}

if(BuyTicketB27==0 && 1270 - Zonepitch*28 < Ask  && Ask< 1270 - Zonepitch*27 ){
	BuyTicketB27 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB27!=0 &&  Bid > 1270 - Zonepitch*26.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB27,SELECT_BY_TICKET);
	OrderClose(BuyTicketB27,OrderLots(),Bid,3,Green);
	BuyTicketB27 = 0;
}

if(BuyTicketB28==0 && 1270 - Zonepitch*29 < Ask  && Ask< 1270 - Zonepitch*28 ){
	BuyTicketB28 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB28!=0 &&  Bid > 1270 - Zonepitch*27.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB28,SELECT_BY_TICKET);
	OrderClose(BuyTicketB28,OrderLots(),Bid,3,Green);
	BuyTicketB28 = 0;
}

if(BuyTicketB29==0 && 1270 - Zonepitch*30 < Ask  && Ask< 1270 - Zonepitch*29 ){
	BuyTicketB29 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB29!=0 &&  Bid > 1270 - Zonepitch*28.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB29,SELECT_BY_TICKET);
	OrderClose(BuyTicketB29,OrderLots(),Bid,3,Green);
	BuyTicketB29 = 0;
}

if(BuyTicketB30==0 && 1270 - Zonepitch*31 < Ask  && Ask< 1270 - Zonepitch*30 ){
	BuyTicketB30 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB30!=0 &&  Bid > 1270 - Zonepitch*29.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB30,SELECT_BY_TICKET);
	OrderClose(BuyTicketB30,OrderLots(),Bid,3,Green);
	BuyTicketB30 = 0;
}

if(BuyTicketB31==0 && 1270 - Zonepitch*32 < Ask  && Ask< 1270 - Zonepitch*31 ){
	BuyTicketB31 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB31!=0 &&  Bid > 1270 - Zonepitch*30.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB31,SELECT_BY_TICKET);
	OrderClose(BuyTicketB31,OrderLots(),Bid,3,Green);
	BuyTicketB31 = 0;
}

if(BuyTicketB32==0 && 1270 - Zonepitch*33 < Ask  && Ask< 1270 - Zonepitch*32 ){
	BuyTicketB32 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB32!=0 &&  Bid > 1270 - Zonepitch*31.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB32,SELECT_BY_TICKET);
	OrderClose(BuyTicketB32,OrderLots(),Bid,3,Green);
	BuyTicketB32 = 0;
}

if(BuyTicketB33==0 && 1270 - Zonepitch*34 < Ask  && Ask< 1270 - Zonepitch*33 ){
	BuyTicketB33 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB33!=0 &&  Bid > 1270 - Zonepitch*32.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB33,SELECT_BY_TICKET);
	OrderClose(BuyTicketB33,OrderLots(),Bid,3,Green);
	BuyTicketB33 = 0;
}

if(BuyTicketB34==0 && 1270 - Zonepitch*35 < Ask  && Ask< 1270 - Zonepitch*34 ){
	BuyTicketB34 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB34!=0 &&  Bid > 1270 - Zonepitch*33.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB34,SELECT_BY_TICKET);
	OrderClose(BuyTicketB34,OrderLots(),Bid,3,Green);
	BuyTicketB34 = 0;
}


if(BuyTicketB35==0 && 1270 - Zonepitch*36 < Ask  && Ask< 1270 - Zonepitch*35 ){
	BuyTicketB35 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB35!=0 &&  Bid > 1270 - Zonepitch*34.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB35,SELECT_BY_TICKET);
	OrderClose(BuyTicketB35,OrderLots(),Bid,3,Green);
	BuyTicketB35 = 0;
}

if(BuyTicketB36==0 && 1270 - Zonepitch*37 < Ask  && Ask< 1270 - Zonepitch*36 ){
	BuyTicketB36 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB36!=0 &&  Bid > 1270 - Zonepitch*35.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB36,SELECT_BY_TICKET);
	OrderClose(BuyTicketB36,OrderLots(),Bid,3,Green);
	BuyTicketB36 = 0;
}

if(BuyTicketB37==0 && 1270 - Zonepitch*38 < Ask  && Ask< 1270 - Zonepitch*37 ){
	BuyTicketB37 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB37!=0 &&  Bid > 1270 - Zonepitch*36.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB37,SELECT_BY_TICKET);
	OrderClose(BuyTicketB37,OrderLots(),Bid,3,Green);
	BuyTicketB37 = 0;
}

if(BuyTicketB38==0 && 1270 - Zonepitch*39 < Ask  && Ask< 1270 - Zonepitch*38 ){
	BuyTicketB38 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB38!=0 &&  Bid > 1270 - Zonepitch*37.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB38,SELECT_BY_TICKET);
	OrderClose(BuyTicketB38,OrderLots(),Bid,3,Green);
	BuyTicketB38 = 0;
}

if(BuyTicketB39==0 && 1270 - Zonepitch*40 < Ask  && Ask< 1270 - Zonepitch*39 ){
	BuyTicketB39 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB39!=0 &&  Bid > 1270 - Zonepitch*38.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB39,SELECT_BY_TICKET);
	OrderClose(BuyTicketB39,OrderLots(),Bid,3,Green);
	BuyTicketB39 = 0;
}

if(BuyTicketB40==0 && 1270 - Zonepitch*41 < Ask  && Ask< 1270 - Zonepitch*40 ){
	BuyTicketB40 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB40!=0 &&  Bid > 1270 - Zonepitch*39.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB40,SELECT_BY_TICKET);
	OrderClose(BuyTicketB40,OrderLots(),Bid,3,Green);
	BuyTicketB40 = 0;
}

if(BuyTicketB41==0 && 1270 - Zonepitch*42 < Ask  && Ask< 1270 - Zonepitch*41 ){
	BuyTicketB41 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB41!=0 &&  Bid > 1270 - Zonepitch*40.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB41,SELECT_BY_TICKET);
	OrderClose(BuyTicketB41,OrderLots(),Bid,3,Green);
	BuyTicketB41 = 0;
}

if(BuyTicketB42==0 && 1270 - Zonepitch*43 < Ask  && Ask< 1270 - Zonepitch*42 ){
	BuyTicketB42 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB42!=0 &&  Bid > 1270 - Zonepitch*41.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB42,SELECT_BY_TICKET);
	OrderClose(BuyTicketB42,OrderLots(),Bid,3,Green);
	BuyTicketB42 = 0;
}

if(BuyTicketB43==0 && 1270 - Zonepitch*44 < Ask  && Ask< 1270 - Zonepitch*43 ){
	BuyTicketB43 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB43!=0 &&  Bid > 1270 - Zonepitch*42.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB43,SELECT_BY_TICKET);
	OrderClose(BuyTicketB43,OrderLots(),Bid,3,Green);
	BuyTicketB43 = 0;
}

if(BuyTicketB44==0 && 1270 - Zonepitch*45 < Ask  && Ask< 1270 - Zonepitch*44 ){
	BuyTicketB44 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB44!=0 &&  Bid > 1270 - Zonepitch*43.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB44,SELECT_BY_TICKET);
	OrderClose(BuyTicketB44,OrderLots(),Bid,3,Green);
	BuyTicketB44 = 0;
}

if(BuyTicketB45==0 && 1270 - Zonepitch*46 < Ask  && Ask< 1270 - Zonepitch*45 ){
	BuyTicketB45 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB45!=0 &&  Bid > 1270 - Zonepitch*44.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB45,SELECT_BY_TICKET);
	OrderClose(BuyTicketB45,OrderLots(),Bid,3,Green);
	BuyTicketB45 = 0;
}

if(BuyTicketB46==0 && 1270 - Zonepitch*47 < Ask  && Ask< 1270 - Zonepitch*46 ){
	BuyTicketB46 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB46!=0 &&  Bid > 1270 - Zonepitch*45.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB46,SELECT_BY_TICKET);
	OrderClose(BuyTicketB46,OrderLots(),Bid,3,Green);
	BuyTicketB46 = 0;
}

if(BuyTicketB47==0 && 1270 - Zonepitch*48 < Ask  && Ask< 1270 - Zonepitch*47 ){
	BuyTicketB47 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB47!=0 &&  Bid > 1270 - Zonepitch*46.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB47,SELECT_BY_TICKET);
	OrderClose(BuyTicketB47,OrderLots(),Bid,3,Green);
	BuyTicketB47 = 0;
}

if(BuyTicketB48==0 && 1270 - Zonepitch*49 < Ask  && Ask< 1270 - Zonepitch*48 ){
	BuyTicketB48 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB48!=0 &&  Bid > 1270 - Zonepitch*47.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB48,SELECT_BY_TICKET);
	OrderClose(BuyTicketB48,OrderLots(),Bid,3,Green);
	BuyTicketB48 = 0;
}

if(BuyTicketB49==0 && 1270 - Zonepitch*50 < Ask  && Ask< 1270 - Zonepitch*49 ){
	BuyTicketB49 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB49!=0 &&  Bid > 1270 - Zonepitch*48.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB49,SELECT_BY_TICKET);
	OrderClose(BuyTicketB49,OrderLots(),Bid,3,Green);
	BuyTicketB49 = 0;
}

if(BuyTicketB50==0 && 1270 - Zonepitch*51 < Ask  && Ask< 1270 - Zonepitch*50 ){
	BuyTicketB50 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB50!=0 &&  Bid > 1270 - Zonepitch*49.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB50,SELECT_BY_TICKET);
	OrderClose(BuyTicketB50,OrderLots(),Bid,3,Green);
	BuyTicketB50 = 0;
}

if(BuyTicketB51==0 && 1270 - Zonepitch*52 < Ask  && Ask< 1270 - Zonepitch*51 ){
	BuyTicketB51 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB51!=0 &&  Bid > 1270 - Zonepitch*50.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB51,SELECT_BY_TICKET);
	OrderClose(BuyTicketB51,OrderLots(),Bid,3,Green);
	BuyTicketB51 = 0;
}

if(BuyTicketB52==0 && 1270 - Zonepitch*53 < Ask  && Ask< 1270 - Zonepitch*52 ){
	BuyTicketB52 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB52!=0 &&  Bid > 1270 - Zonepitch*51.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB52,SELECT_BY_TICKET);
	OrderClose(BuyTicketB52,OrderLots(),Bid,3,Green);
	BuyTicketB52 = 0;
}

if(BuyTicketB53==0 && 1270 - Zonepitch*54 < Ask  && Ask< 1270 - Zonepitch*53 ){
	BuyTicketB53 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB53!=0 &&  Bid > 1270 - Zonepitch*52.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB53,SELECT_BY_TICKET);
	OrderClose(BuyTicketB53,OrderLots(),Bid,3,Green);
	BuyTicketB53 = 0;
}

if(BuyTicketB54==0 && 1270 - Zonepitch*55 < Ask  && Ask< 1270 - Zonepitch*54 ){
	BuyTicketB54 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB54!=0 &&  Bid > 1270 - Zonepitch*53.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB54,SELECT_BY_TICKET);
	OrderClose(BuyTicketB54,OrderLots(),Bid,3,Green);
	BuyTicketB54 = 0;
}

if(BuyTicketB55==0 && 1270 - Zonepitch*56 < Ask  && Ask< 1270 - Zonepitch*55 ){
	BuyTicketB55 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB55!=0 &&  Bid > 1270 - Zonepitch*54.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB55,SELECT_BY_TICKET);
	OrderClose(BuyTicketB55,OrderLots(),Bid,3,Green);
	BuyTicketB55 = 0;
}

if(BuyTicketB56==0 && 1270 - Zonepitch*57 < Ask  && Ask< 1270 - Zonepitch*56 ){
	BuyTicketB56 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB56!=0 &&  Bid > 1270 - Zonepitch*55.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB56,SELECT_BY_TICKET);
	OrderClose(BuyTicketB56,OrderLots(),Bid,3,Green);
	BuyTicketB56 = 0;
}

if(BuyTicketB57==0 && 1270 - Zonepitch*58 < Ask  && Ask< 1270 - Zonepitch*57 ){
	BuyTicketB57 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB57!=0 &&  Bid > 1270 - Zonepitch*56.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB57,SELECT_BY_TICKET);
	OrderClose(BuyTicketB57,OrderLots(),Bid,3,Green);
	BuyTicketB57 = 0;
}

if(BuyTicketB58==0 && 1270 - Zonepitch*59 < Ask  && Ask< 1270 - Zonepitch*58 ){
	BuyTicketB58 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB58!=0 &&  Bid > 1270 - Zonepitch*57.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB58,SELECT_BY_TICKET);
	OrderClose(BuyTicketB58,OrderLots(),Bid,3,Green);
	BuyTicketB58 = 0;
}

if(BuyTicketB59==0 && 1270 - Zonepitch*60 < Ask  && Ask< 1270 - Zonepitch*59 ){
	BuyTicketB59 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB59!=0 &&  Bid > 1270 - Zonepitch*58.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB59,SELECT_BY_TICKET);
	OrderClose(BuyTicketB59,OrderLots(),Bid,3,Green);
	BuyTicketB59 = 0;
}

if(BuyTicketB60==0 && 1270 - Zonepitch*61 < Ask  && Ask< 1270 - Zonepitch*60 ){
	BuyTicketB60 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketB60!=0 &&  Bid > 1270 - Zonepitch*59.8  && Bid > EMA100 ){
	OrderSelect(BuyTicketB60,SELECT_BY_TICKET);
	OrderClose(BuyTicketB60,OrderLots(),Bid,3,Green);
	BuyTicketB60 = 0;
}

if(BuyTicketC1==0 && 1270 - Zonepitch*2 < Ask  && Ask< 1270 - Zonepitch*1 ){
	BuyTicketC1 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC1!=0 &&  Bid > 1270 - Zonepitch*0.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC1,SELECT_BY_TICKET);
	OrderClose(BuyTicketC1,OrderLots(),Bid,3,Green);
	BuyTicketC1 = 0;
}

if(BuyTicketC2==0 && 1400 - Zonepitch*3 < Ask  && Ask< 1400 - Zonepitch*2 ){
	BuyTicketC2 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC2!=0 &&  Bid > 1400 - Zonepitch*1.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC2,SELECT_BY_TICKET);
	OrderClose(BuyTicketC2,OrderLots(),Bid,3,Green);
	BuyTicketC2 = 0;
}

if(BuyTicketC3==0 && 1400 - Zonepitch*4 < Ask  && Ask< 1400 - Zonepitch*3 ){
	BuyTicketC3 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC3!=0 &&  Bid > 1400 - Zonepitch*2.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC3,SELECT_BY_TICKET);
	OrderClose(BuyTicketC3,OrderLots(),Bid,3,Green);
	BuyTicketC3 = 0;
}

if(BuyTicketC4==0 && 1400 - Zonepitch*5 < Ask  && Ask< 1400 - Zonepitch*4 ){
	BuyTicketC4 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC4!=0 &&  Bid > 1400 - Zonepitch*3.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC4,SELECT_BY_TICKET);
	OrderClose(BuyTicketC4,OrderLots(),Bid,3,Green);
	BuyTicketC4 = 0;
}

if(BuyTicketC5==0 && 1400 - Zonepitch*6 < Ask  && Ask< 1400 - Zonepitch*5 ){
	BuyTicketC5 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC5!=0 &&  Bid > 1400 - Zonepitch*4.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC5,SELECT_BY_TICKET);
	OrderClose(BuyTicketC5,OrderLots(),Bid,3,Green);
	BuyTicketC5 = 0;
}

if(BuyTicketC6==0 && 1400 - Zonepitch*7 < Ask  && Ask< 1400 - Zonepitch*6 ){
	BuyTicketC6 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC6!=0 &&  Bid > 1400 - Zonepitch*5.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC6,SELECT_BY_TICKET);
	OrderClose(BuyTicketC6,OrderLots(),Bid,3,Green);
	BuyTicketC6 = 0;
}

if(BuyTicketC7==0 && 1400 - Zonepitch*8 < Ask  && Ask< 1400 - Zonepitch*7 ){
	BuyTicketC7 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC7!=0 &&  Bid > 1400 - Zonepitch*6.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC7,SELECT_BY_TICKET);
	OrderClose(BuyTicketC7,OrderLots(),Bid,3,Green);
	BuyTicketC7 = 0;
}

if(BuyTicketC8==0 && 1400 - Zonepitch*9 < Ask  && Ask< 1400 - Zonepitch*8 ){
	BuyTicketC8 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC8!=0 &&  Bid > 1400 - Zonepitch*7.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC8,SELECT_BY_TICKET);
	OrderClose(BuyTicketC8,OrderLots(),Bid,3,Green);
	BuyTicketC8 = 0;
}

if(BuyTicketC9==0 && 1400 - Zonepitch*10 < Ask  && Ask< 1400 - Zonepitch*9 ){
	BuyTicketC9 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC9!=0 &&  Bid > 1400 - Zonepitch*8.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC9,SELECT_BY_TICKET);
	OrderClose(BuyTicketC9,OrderLots(),Bid,3,Green);
	BuyTicketC9 = 0;
}

if(BuyTicketC10==0 && 1400 - Zonepitch*11 < Ask  && Ask< 1400 - Zonepitch*10 ){
	BuyTicketC10 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC10!=0 &&  Bid > 1400 - Zonepitch*9.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC10,SELECT_BY_TICKET);
	OrderClose(BuyTicketC10,OrderLots(),Bid,3,Green);
	BuyTicketC10 = 0;
}

if(BuyTicketC11==0 && 1400 - Zonepitch*12 < Ask  && Ask< 1400 - Zonepitch*11 ){
	BuyTicketC11 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC11!=0 &&  Bid > 1400 - Zonepitch*10.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC11,SELECT_BY_TICKET);
	OrderClose(BuyTicketC11,OrderLots(),Bid,3,Green);
	BuyTicketC11 = 0;
}

if(BuyTicketC12==0 && 1400 - Zonepitch*13 < Ask  && Ask< 1400 - Zonepitch*12 ){
	BuyTicketC12 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC12!=0 &&  Bid > 1400 - Zonepitch*11.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC12,SELECT_BY_TICKET);
	OrderClose(BuyTicketC12,OrderLots(),Bid,3,Green);
	BuyTicketC12 = 0;
}

if(BuyTicketC13==0 && 1400 - Zonepitch*14 < Ask  && Ask< 1400 - Zonepitch*13 ){
	BuyTicketC13 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC13!=0 &&  Bid > 1400 - Zonepitch*12.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC13,SELECT_BY_TICKET);
	OrderClose(BuyTicketC13,OrderLots(),Bid,3,Green);
	BuyTicketC13 = 0;
}

if(BuyTicketC14==0 && 1400 - Zonepitch*15 < Ask  && Ask< 1400 - Zonepitch*14 ){
	BuyTicketC14 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC14!=0 &&  Bid > 1400 - Zonepitch*13.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC14,SELECT_BY_TICKET);
	OrderClose(BuyTicketC14,OrderLots(),Bid,3,Green);
	BuyTicketC14 = 0;
}

if(BuyTicketC15==0 && 1400 - Zonepitch*16 < Ask  && Ask< 1400 - Zonepitch*15 ){
	BuyTicketC15 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC15!=0 &&  Bid > 1400 - Zonepitch*14.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC15,SELECT_BY_TICKET);
	OrderClose(BuyTicketC15,OrderLots(),Bid,3,Green);
	BuyTicketC15 = 0;
}

if(BuyTicketC16==0 && 1400 - Zonepitch*17 < Ask  && Ask< 1400 - Zonepitch*16 ){
	BuyTicketC16 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC16!=0 &&  Bid > 1400 - Zonepitch*15.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC16,SELECT_BY_TICKET);
	OrderClose(BuyTicketC16,OrderLots(),Bid,3,Green);
	BuyTicketC16 = 0;
}

if(BuyTicketC17==0 && 1400 - Zonepitch*18 < Ask  && Ask< 1400 - Zonepitch*17 ){
	BuyTicketC17 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC17!=0 &&  Bid > 1400 - Zonepitch*16.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC17,SELECT_BY_TICKET);
	OrderClose(BuyTicketC17,OrderLots(),Bid,3,Green);
	BuyTicketC17 = 0;
}

if(BuyTicketC18==0 && 1400 - Zonepitch*19 < Ask  && Ask< 1400 - Zonepitch*18 ){
	BuyTicketC18 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC18!=0 &&  Bid > 1400 - Zonepitch*17.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC18,SELECT_BY_TICKET);
	OrderClose(BuyTicketC18,OrderLots(),Bid,3,Green);
	BuyTicketC18 = 0;
}

if(BuyTicketC19==0 && 1400 - Zonepitch*20 < Ask  && Ask< 1400 - Zonepitch*19 ){
	BuyTicketC19 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC19!=0 &&  Bid > 1400 - Zonepitch*18.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC19,SELECT_BY_TICKET);
	OrderClose(BuyTicketC19,OrderLots(),Bid,3,Green);
	BuyTicketC19 = 0;
}

if(BuyTicketC20==0 && 1400 - Zonepitch*21 < Ask  && Ask< 1400 - Zonepitch*20 ){
	BuyTicketC20 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC20!=0 &&  Bid > 1400 - Zonepitch*19.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC20,SELECT_BY_TICKET);
	OrderClose(BuyTicketC20,OrderLots(),Bid,3,Green);
	BuyTicketC20 = 0;
}

if(BuyTicketC21==0 && 1400 - Zonepitch*22 < Ask  && Ask< 1400 - Zonepitch*21 ){
	BuyTicketC21 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC21!=0 &&  Bid > 1400 - Zonepitch*20.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC21,SELECT_BY_TICKET);
	OrderClose(BuyTicketC21,OrderLots(),Bid,3,Green);
	BuyTicketC21 = 0;
}

if(BuyTicketC22==0 && 1400 - Zonepitch*23 < Ask  && Ask< 1400 - Zonepitch*22 ){
	BuyTicketC22 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC22!=0 &&  Bid > 1400 - Zonepitch*21.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC22,SELECT_BY_TICKET);
	OrderClose(BuyTicketC22,OrderLots(),Bid,3,Green);
	BuyTicketC22 = 0;
}

if(BuyTicketC23==0 && 1400 - Zonepitch*24 < Ask  && Ask< 1400 - Zonepitch*23 ){
	BuyTicketC23 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC23!=0 &&  Bid > 1400 - Zonepitch*22.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC23,SELECT_BY_TICKET);
	OrderClose(BuyTicketC23,OrderLots(),Bid,3,Green);
	BuyTicketC23 = 0;
}

if(BuyTicketC24==0 && 1400 - Zonepitch*25 < Ask  && Ask< 1400 - Zonepitch*24 ){
	BuyTicketC24 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC24!=0 &&  Bid > 1400 - Zonepitch*23.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC24,SELECT_BY_TICKET);
	OrderClose(BuyTicketC24,OrderLots(),Bid,3,Green);
	BuyTicketC24 = 0;
}

if(BuyTicketC25==0 && 1400 - Zonepitch*26 < Ask  && Ask< 1400 - Zonepitch*25 ){
	BuyTicketC25 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC25!=0 &&  Bid > 1400 - Zonepitch*24.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC25,SELECT_BY_TICKET);
	OrderClose(BuyTicketC25,OrderLots(),Bid,3,Green);
	BuyTicketC25 = 0;
}

if(BuyTicketC26==0 && 1400 - Zonepitch*27 < Ask  && Ask< 1400 - Zonepitch*26 ){
	BuyTicketC26 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC26!=0 &&  Bid > 1400 - Zonepitch*25.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC26,SELECT_BY_TICKET);
	OrderClose(BuyTicketC26,OrderLots(),Bid,3,Green);
	BuyTicketC26 = 0;
}

if(BuyTicketC27==0 && 1400 - Zonepitch*28 < Ask  && Ask< 1400 - Zonepitch*27 ){
	BuyTicketC27 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC27!=0 &&  Bid > 1400 - Zonepitch*26.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC27,SELECT_BY_TICKET);
	OrderClose(BuyTicketC27,OrderLots(),Bid,3,Green);
	BuyTicketC27 = 0;
}

if(BuyTicketC28==0 && 1400 - Zonepitch*29 < Ask  && Ask< 1400 - Zonepitch*28 ){
	BuyTicketC28 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC28!=0 &&  Bid > 1400 - Zonepitch*27.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC28,SELECT_BY_TICKET);
	OrderClose(BuyTicketC28,OrderLots(),Bid,3,Green);
	BuyTicketC28 = 0;
}

if(BuyTicketC29==0 && 1400 - Zonepitch*30 < Ask  && Ask< 1400 - Zonepitch*29 ){
	BuyTicketC29 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC29!=0 &&  Bid > 1400 - Zonepitch*28.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC29,SELECT_BY_TICKET);
	OrderClose(BuyTicketC29,OrderLots(),Bid,3,Green);
	BuyTicketC29 = 0;
}

if(BuyTicketC30==0 && 1400 - Zonepitch*31 < Ask  && Ask< 1400 - Zonepitch*30 ){
	BuyTicketC30 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC30!=0 &&  Bid > 1400 - Zonepitch*29.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC30,SELECT_BY_TICKET);
	OrderClose(BuyTicketC30,OrderLots(),Bid,3,Green);
	BuyTicketC30 = 0;
}

if(BuyTicketC31==0 && 1400 - Zonepitch*32 < Ask  && Ask< 1400 - Zonepitch*31 ){
	BuyTicketC31 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC31!=0 &&  Bid > 1400 - Zonepitch*30.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC31,SELECT_BY_TICKET);
	OrderClose(BuyTicketC31,OrderLots(),Bid,3,Green);
	BuyTicketC31 = 0;
}

if(BuyTicketC32==0 && 1400 - Zonepitch*33 < Ask  && Ask< 1400 - Zonepitch*32 ){
	BuyTicketC32 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC32!=0 &&  Bid > 1400 - Zonepitch*31.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC32,SELECT_BY_TICKET);
	OrderClose(BuyTicketC32,OrderLots(),Bid,3,Green);
	BuyTicketC32 = 0;
}

if(BuyTicketC33==0 && 1400 - Zonepitch*34 < Ask  && Ask< 1400 - Zonepitch*33 ){
	BuyTicketC33 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC33!=0 &&  Bid > 1400 - Zonepitch*32.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC33,SELECT_BY_TICKET);
	OrderClose(BuyTicketC33,OrderLots(),Bid,3,Green);
	BuyTicketC33 = 0;
}

if(BuyTicketC34==0 && 1400 - Zonepitch*35 < Ask  && Ask< 1400 - Zonepitch*34 ){
	BuyTicketC34 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC34!=0 &&  Bid > 1400 - Zonepitch*33.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC34,SELECT_BY_TICKET);
	OrderClose(BuyTicketC34,OrderLots(),Bid,3,Green);
	BuyTicketC34 = 0;
}

if(BuyTicketC35==0 && 1400 - Zonepitch*36 < Ask  && Ask< 1400 - Zonepitch*35 ){
	BuyTicketC35 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC35!=0 &&  Bid > 1400 - Zonepitch*34.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC35,SELECT_BY_TICKET);
	OrderClose(BuyTicketC35,OrderLots(),Bid,3,Green);
	BuyTicketC35 = 0;
}

if(BuyTicketC36==0 && 1400 - Zonepitch*37 < Ask  && Ask< 1400 - Zonepitch*36 ){
	BuyTicketC36 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC36!=0 &&  Bid > 1400 - Zonepitch*35.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC36,SELECT_BY_TICKET);
	OrderClose(BuyTicketC36,OrderLots(),Bid,3,Green);
	BuyTicketC36 = 0;
}

if(BuyTicketC37==0 && 1400 - Zonepitch*38 < Ask  && Ask< 1400 - Zonepitch*37 ){
	BuyTicketC37 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC37!=0 &&  Bid > 1400 - Zonepitch*36.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC37,SELECT_BY_TICKET);
	OrderClose(BuyTicketC37,OrderLots(),Bid,3,Green);
	BuyTicketC37 = 0;
}

if(BuyTicketC38==0 && 1400 - Zonepitch*39 < Ask  && Ask< 1400 - Zonepitch*38 ){
	BuyTicketC38 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC38!=0 &&  Bid > 1400 - Zonepitch*37.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC38,SELECT_BY_TICKET);
	OrderClose(BuyTicketC38,OrderLots(),Bid,3,Green);
	BuyTicketC38 = 0;
}

if(BuyTicketC39==0 && 1400 - Zonepitch*40 < Ask  && Ask< 1400 - Zonepitch*39 ){
	BuyTicketC39 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC39!=0 &&  Bid > 1400 - Zonepitch*38.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC39,SELECT_BY_TICKET);
	OrderClose(BuyTicketC39,OrderLots(),Bid,3,Green);
	BuyTicketC39 = 0;
}

if(BuyTicketC40==0 && 1400 - Zonepitch*41 < Ask  && Ask< 1400 - Zonepitch*40 ){
	BuyTicketC40 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC40!=0 &&  Bid > 1400 - Zonepitch*39.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC40,SELECT_BY_TICKET);
	OrderClose(BuyTicketC40,OrderLots(),Bid,3,Green);
	BuyTicketC40 = 0;
}

if(BuyTicketC41==0 && 1400 - Zonepitch*42 < Ask  && Ask< 1400 - Zonepitch*41 ){
	BuyTicketC41 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC41!=0 &&  Bid > 1400 - Zonepitch*40.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC41,SELECT_BY_TICKET);
	OrderClose(BuyTicketC41,OrderLots(),Bid,3,Green);
	BuyTicketC41 = 0;
}

if(BuyTicketC42==0 && 1400 - Zonepitch*43 < Ask  && Ask< 1400 - Zonepitch*42 ){
	BuyTicketC42 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC42!=0 &&  Bid > 1400 - Zonepitch*41.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC42,SELECT_BY_TICKET);
	OrderClose(BuyTicketC42,OrderLots(),Bid,3,Green);
	BuyTicketC42 = 0;
}

if(BuyTicketC43==0 && 1400 - Zonepitch*44 < Ask  && Ask< 1400 - Zonepitch*43 ){
	BuyTicketC43 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC43!=0 &&  Bid > 1400 - Zonepitch*42.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC43,SELECT_BY_TICKET);
	OrderClose(BuyTicketC43,OrderLots(),Bid,3,Green);
	BuyTicketC43 = 0;
}

if(BuyTicketC44==0 && 1400 - Zonepitch*45 < Ask  && Ask< 1400 - Zonepitch*44 ){
	BuyTicketC44 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC44!=0 &&  Bid > 1400 - Zonepitch*43.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC44,SELECT_BY_TICKET);
	OrderClose(BuyTicketC44,OrderLots(),Bid,3,Green);
	BuyTicketC44 = 0;
}

if(BuyTicketC45==0 && 1400 - Zonepitch*46 < Ask  && Ask< 1400 - Zonepitch*45 ){
	BuyTicketC45 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC45!=0 &&  Bid > 1400 - Zonepitch*44.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC45,SELECT_BY_TICKET);
	OrderClose(BuyTicketC45,OrderLots(),Bid,3,Green);
	BuyTicketC45 = 0;
}

if(BuyTicketC46==0 && 1400 - Zonepitch*47 < Ask  && Ask< 1400 - Zonepitch*46 ){
	BuyTicketC46 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC46!=0 &&  Bid > 1400 - Zonepitch*45.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC46,SELECT_BY_TICKET);
	OrderClose(BuyTicketC46,OrderLots(),Bid,3,Green);
	BuyTicketC46 = 0;
}

if(BuyTicketC47==0 && 1400 - Zonepitch*48 < Ask  && Ask< 1400 - Zonepitch*47 ){
	BuyTicketC47 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC47!=0 &&  Bid > 1400 - Zonepitch*46.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC47,SELECT_BY_TICKET);
	OrderClose(BuyTicketC47,OrderLots(),Bid,3,Green);
	BuyTicketC47 = 0;
}

if(BuyTicketC48==0 && 1400 - Zonepitch*49 < Ask  && Ask< 1400 - Zonepitch*48 ){
	BuyTicketC48 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC48!=0 &&  Bid > 1400 - Zonepitch*47.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC48,SELECT_BY_TICKET);
	OrderClose(BuyTicketC48,OrderLots(),Bid,3,Green);
	BuyTicketC48 = 0;
}

if(BuyTicketC49==0 && 1400 - Zonepitch*50 < Ask  && Ask< 1400 - Zonepitch*49 ){
	BuyTicketC49 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC49!=0 &&  Bid > 1400 - Zonepitch*48.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC49,SELECT_BY_TICKET);
	OrderClose(BuyTicketC49,OrderLots(),Bid,3,Green);
	BuyTicketC49 = 0;
}

if(BuyTicketC50==0 && 1400 - Zonepitch*51 < Ask  && Ask< 1400 - Zonepitch*50 ){
	BuyTicketC50 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC50!=0 &&  Bid > 1400 - Zonepitch*49.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC50,SELECT_BY_TICKET);
	OrderClose(BuyTicketC50,OrderLots(),Bid,3,Green);
	BuyTicketC50 = 0;
}

if(BuyTicketC51==0 && 1400 - Zonepitch*52 < Ask  && Ask< 1400 - Zonepitch*51 ){
	BuyTicketC51 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC51!=0 &&  Bid > 1400 - Zonepitch*50.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC51,SELECT_BY_TICKET);
	OrderClose(BuyTicketC51,OrderLots(),Bid,3,Green);
	BuyTicketC51 = 0;
}

if(BuyTicketC52==0 && 1400 - Zonepitch*53 < Ask  && Ask< 1400 - Zonepitch*52 ){
	BuyTicketC52 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC52!=0 &&  Bid > 1400 - Zonepitch*51.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC52,SELECT_BY_TICKET);
	OrderClose(BuyTicketC52,OrderLots(),Bid,3,Green);
	BuyTicketC52 = 0;
}

if(BuyTicketC53==0 && 1400 - Zonepitch*54 < Ask  && Ask< 1400 - Zonepitch*53 ){
	BuyTicketC53 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC53!=0 &&  Bid > 1400 - Zonepitch*52.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC53,SELECT_BY_TICKET);
	OrderClose(BuyTicketC53,OrderLots(),Bid,3,Green);
	BuyTicketC53 = 0;
}

if(BuyTicketC54==0 && 1400 - Zonepitch*55 < Ask  && Ask< 1400 - Zonepitch*54 ){
	BuyTicketC54 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC54!=0 &&  Bid > 1400 - Zonepitch*53.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC54,SELECT_BY_TICKET);
	OrderClose(BuyTicketC54,OrderLots(),Bid,3,Green);
	BuyTicketC54 = 0;
}

if(BuyTicketC55==0 && 1400 - Zonepitch*56 < Ask  && Ask< 1400 - Zonepitch*55 ){
	BuyTicketC55 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC55!=0 &&  Bid > 1400 - Zonepitch*54.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC55,SELECT_BY_TICKET);
	OrderClose(BuyTicketC55,OrderLots(),Bid,3,Green);
	BuyTicketC55 = 0;
}

if(BuyTicketC56==0 && 1400 - Zonepitch*57 < Ask  && Ask< 1400 - Zonepitch*56 ){
	BuyTicketC56 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC56!=0 &&  Bid > 1400 - Zonepitch*55.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC56,SELECT_BY_TICKET);
	OrderClose(BuyTicketC56,OrderLots(),Bid,3,Green);
	BuyTicketC56 = 0;
}

if(BuyTicketC57==0 && 1400 - Zonepitch*58 < Ask  && Ask< 1400 - Zonepitch*57 ){
	BuyTicketC57 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC57!=0 &&  Bid > 1400 - Zonepitch*56.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC57,SELECT_BY_TICKET);
	OrderClose(BuyTicketC57,OrderLots(),Bid,3,Green);
	BuyTicketC57 = 0;
}

if(BuyTicketC58==0 && 1400 - Zonepitch*59 < Ask  && Ask< 1400 - Zonepitch*58 ){
	BuyTicketC58 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC58!=0 &&  Bid > 1400 - Zonepitch*57.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC58,SELECT_BY_TICKET);
	OrderClose(BuyTicketC58,OrderLots(),Bid,3,Green);
	BuyTicketC58 = 0;
}

if(BuyTicketC59==0 && 1400 - Zonepitch*60 < Ask  && Ask< 1400 - Zonepitch*59 ){
	BuyTicketC59 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC59!=0 &&  Bid > 1400 - Zonepitch*58.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC59,SELECT_BY_TICKET);
	OrderClose(BuyTicketC59,OrderLots(),Bid,3,Green);
	BuyTicketC59 = 0;
}

if(BuyTicketC60==0 && 1400 - Zonepitch*61 < Ask  && Ask< 1400 - Zonepitch*60 ){
	BuyTicketC60 = OrderSend(Symbol(),OP_BUY,Lot,Ask, 3 ,0,0,0,MagicNumber,0,Green);
}
if(BuyTicketC60!=0 &&  Bid > 1400 - Zonepitch*59.8  && Bid > EMA100  && NewBar && Bid >UPBB  ){
	OrderSelect(BuyTicketC60,SELECT_BY_TICKET);
	OrderClose(BuyTicketC60,OrderLots(),Bid,3,Green);
	BuyTicketC60 = 0;
}

	Comment("Upper BB Value = ", DoubleToStr(UPBB,2) ,"\n","Lower BB Value = ",DoubleToStr(LOBB,2),"\n","Stochastic Value = ",DoubleToStr(Sto,2),
	"\n","Total Order Count = ",TotalOrderCount(Symbol(),MagicNumber),
	"\n","Buy Market Count = " , BuyMarketCount(Symbol(),MagicNumber),
	"\n","Sell Market Count = ", SellMarketCount(Symbol(),MagicNumber),
	"\n","Current Sell Profit = " ,CurrentSellProfit
	,"\n","Current Buy Profit = " ,CurrentBuyProfit
	,"\n","Ask = " , DoubleToStr(Ask,2),
	"\n","Account Equity = " , AccountEquity() );

}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| expert Other function                                            |
//+------------------------------------------------------------------+

// Order Count Function
int TotalOrderCount(string argSymbol, int argMagicNumber){
	int TTOrderCount;
	for(int Counter = 0; Counter <= OrdersTotal()-1; Counter++){
		OrderSelect(Counter,SELECT_BY_POS);
		if(OrderMagicNumber() == argMagicNumber && OrderSymbol() == argSymbol){
			TTOrderCount++;
		}
	}
	return(TTOrderCount);
	TTOrderCount=0;
}
//+------------------------------------------------------------------+

// Buy Order Count Function
int BuyMarketCount(string argSymbol, int argMagicNumber){
	int BOrderCount;
	for(int Counter = 0; Counter <= OrdersTotal()-1; Counter++){
		OrderSelect(Counter,SELECT_BY_POS);
		if(OrderMagicNumber() == argMagicNumber && OrderSymbol() == argSymbol && OrderType() == OP_BUY){
			BOrderCount++;
		}
	}
	return(BOrderCount);
	BOrderCount=0;
}
//+------------------------------------------------------------------+

// Sell Order Count Function
int SellMarketCount(string argSymbol, int argMagicNumber){
	int SOrderCount;
	for(int Counter = 0; Counter <= OrdersTotal()-1; Counter++){
	OrderSelect(Counter,SELECT_BY_POS);
		if(OrderMagicNumber() == argMagicNumber && OrderSymbol() == argSymbol && OrderType() == OP_SELL){
			SOrderCount++;
		}
	}
	return(SOrderCount);
	SOrderCount=0;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
double CurrentSellProfitfn( int SellTicketfn1,int SellTicketfn2,int SellTicketfn3,int SellTicketfn4,int SellTicketfn5  ){
	double ProfitS1 = 0,ProfitS2 = 0,ProfitS3 = 0,ProfitS4 = 0;
	double SellProfit;

	if(SellMarketCount(Symbol(),MagicNumber) == 0){
		SellProfit = 0; 
	}else if(SellMarketCount(Symbol(),MagicNumber) == 1){
		SellProfit= 0; 
		OrderSelect(SellTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 2){
		SellProfit= 0; 
		OrderSelect(SellTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 3){
		SellProfit = 0; 
		OrderSelect(SellTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 4){
		SellProfit = 0; 
		OrderSelect(SellTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn4,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}else if(SellMarketCount(Symbol(),MagicNumber) == 5){
		SellProfit = 0; 
		OrderSelect(SellTicketfn1,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn2,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn3,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn4,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
		OrderSelect(SellTicketfn5,SELECT_BY_TICKET);
		SellProfit = SellProfit + OrderProfit();
	}
	return(SellProfit);
	SellProfit=0;
}
//+------------------------------------------------------------------+

double CurrentBuyProfitfn(int BuyTicketfn1,int BuyTicketfn2,int BuyTicketfn3,int BuyTicketfn4,int BuyTicketfn5){
	double BuyProfit;
	if(BuyMarketCount(Symbol(),MagicNumber) == 0){
		BuyProfit = 0; 
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 1){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 2){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 3){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 4){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn4,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}else if(BuyMarketCount(Symbol(),MagicNumber) == 5){
		BuyProfit = 0; 
		OrderSelect(BuyTicketfn1,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn2,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn3,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn4,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
		OrderSelect(BuyTicketfn5,SELECT_BY_TICKET);
		BuyProfit = BuyProfit + OrderProfit();
	}
	return(BuyProfit);
	BuyProfit=0;
}